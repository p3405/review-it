<img src="https://webassets.telerikacademy.com/images/default-source/logos/telerik-academy.svg)" alt="logo" width="300px" style="margin-top: 20px; float:right"/>

# ReviewIT - a work evaluation and peer-review system

## Description

The members of every organization are responsible for the quality of the products. One mechanism for self-regulation and
coverage of the organization's standards is the peer work review system. This could be in many forms, but in general it
is a peer review of the submitted work item - code, tests, documents, music pieces, academic papers, etc. ReviewIT is a
web API whose primary goal is to allow you to submit pieces of work of any kind and have your team of reviewers
peer-review it.

The workflow is usually the following: the user submits some work, assigns a reviewer, the reviewer can comment,
approve, request changes, or decline the work item. Once the reviewer approves the work item it is marked as complete.

### Built With

Here's a bit of information on everything used to build this API:

* [Java (Source code)](https://www.java.com/en/)
* [SpringMVC (Framework)](https://spring.io/)
* [Hibernate (ORM)](https://hibernate.org/)
* [MariaDB (Database)](https://mariadb.org/)
* [Mockito (Layered Architecture Unit Testing)](https://site.mockito.org/)
* [JUnit v5.4.0 (API Unit Testing)](https://junit.org/junit5/docs/5.4.0/release-notes/)
* [Bootstrap](https://getbootstrap.com)

## Getting Started

Here's what you need to start using our API:

### Prerequisites

* A standard IDE capable of running Java code
* A running database server to connect to

### Setup

_Here are a few of the things you need to do to set it up and get it running:_

1. Clone the <a href="https://gitlab.com/p3405/review-it">repo</a>
   ```sh
   git clone https://gitlab.com/p3405/review-it.git
   ```
2. Connect to a database server
3. Run the API through the `ReviewItApplication.java` file
4. Get familiar with our API REST and MVC endpoints by reading the <a href="http://localhost:8080/swagger-ui.html">
   swagger documentation</a>

### **Example use case:**

A prominent scientist studying the effects and benefits of new experimental drug for Alzheimer, want to make sure that
their research paper is up to standard before publishing it anywhere online. They have heard about your platform and
have created their registrations. They create a new team and add other well-known, leading scientists who work in the
same field. The scientist starts uploading their research, anything from where they got their data from, what methods of
gathering information they have used to full-fledged research papers. They add one reviewer for each work item. Then
their peers start reviewing their work items for errors. Anything from grammar and spelling to more detail-oriented
specifics surrounding their research paper/work item. The peers add comments and can request changes if they deem
necessary. Once the peers are satisfied with the work item they are assigned to, they accept it. The scientist then
gathers all the work items in a full-fledged research paper and is ready to publish it online!

## Database

_Here's a diagram of our database relations and structure: <br>
<img src="https://i.imgur.com/DLRL6kf.png">_

## Supported models and their details

The models this application supports are as follows:

- **User:**
    - Must have a username between 2 and 20 symbols long
    - Password must be at least 8 symbols and should contain capital letter, digit, and special symbol (+, -, *, &, ^,
      …)
    - Email must be valid and unique in the system
    - Phone number must be 10 digits long and unique in the system
- **Work Item:**
    - Title must be between 10 and 80 symbols long
    - Description must be between 20 and 20,000 symbols long
    - Each work item must have a reviewer assigned to it
    - Each work item belongs only to a single team
- **Team:**
    - Each team must have an id, name (between 3 and 30 symbols, unique), owner, list of team members, and list of work
      items

#### _**Public part:**_

The public part of the API is freely accessible to anyone without having to register and displays some summarized
information about the API as well as select statistics from our database

#### _**Private part:**_

The API supports registration of a new account, paired with email confirmation. Upon registration, the user can log in
and gains access to additional features in the sidebar.

- Dashboard
    - Contains summarized statistics about the user and their team
- My Teams
    - Expandable menu that contains all the teams this user is part of. Each team is further expandable and allows
      access to the Team Members and Team Items sections
        - Team Members displays a list of all members in this team. If the user is the owner of this team, they can
          remove all other members from the team except themselves.
        - Team Items displays a list of all items assigned to this team.
- My Requests
    - Displays a list of all requests created by this user
- My Reviews
    - Displays a list of all requests assigned to this user
- Create Team
    - Allows the user to create a brand-new team
- F.A.Q
    - An FAQ section that gives an answer to some of the more frequently asked questions about our platform
- Contact
    - A contact form through which the user can reach out to the developers

## REST API

The application also contains a REST API that allows the following operations:

- Users
    - Create
    - Read
    - Update
    - Delete
    - Search by username, email, or phone
- Work Items
    - Create
    - Read
    - Update
    - Delete
    - List – by Status
    - List – by User
    - Filter by title, status, reviewer
    - Sort by id, title, or status
- Teams
    - Create
    - Read
    - Update
    - Delete
    - Add new members to a team
    - Remove members from the team
    - List team members

## Roadmap

- [ ] Add Changelog
- [ ] Extend main functionality
- [ ] Add Additional Templates w/ Examples
- [ ] Add "components" document to easily copy & paste sections of the readme
- [ ] Multi-language Support
- [ ] Work Item versioning
- [ ] Friend invitations
- [ ] Drafts

<!-- CONTRIBUTING -->

## Contributing

Contributions are what make the open source community such an amazing place to learn, inspire, and create. Any
contributions you make are **greatly appreciated**.

If you have a suggestion that would make this better, please fork the repo and create a pull request. You can also
simply open an issue with the tag "enhancement". Don't forget to give the project a star! Thanks again!

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

## License

Distributed freely under <a href="https://opensource.org/licenses/OSL-3.0">OSL 3.0</a>

## Contact

Deyan Stanchev - <a href="mailto: deyan.stanchev.a33@learn.telerikacademy.com">
deyan.stanchev.a33@learn.telerikacademy.com</a> <br>
Hristo Naydenov - <a href="mailto: hristo.naydenov.a33@learn.telerikacademy.com">
hristo.naydenov.a33@learn.telerikacademy.com</a>