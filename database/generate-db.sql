drop database if exists `source-database`;
create database if not exists `source-database`;
use `source-database`;
create table statuses
(
    id   int auto_increment,
    name varchar(20) not null,
    constraint statuses_id_uindex
        unique (id),
    constraint statuses_name_uindex
        unique (name)
);

alter table statuses
    add primary key (id);

create table users
(
    id           int auto_increment,
    username     varchar(20)          not null,
    password     varchar(30)          not null,
    email        varchar(30)          not null,
    phone_number varchar(10)          not null,
    photo_url    varchar(200)         null,
    enabled      tinyint(1)           not null,
    deleted_flag tinyint(1) default 0 not null,
    constraint users_email_uindex
        unique (email),
    constraint users_phone_number_uindex
        unique (phone_number),
    constraint users_user_id_uindex
        unique (id),
    constraint users_username_uindex
        unique (username)
);

alter table users
    add primary key (id);

create table confirmation_tokens
(
    token_id    int auto_increment,
    token       varchar(200) not null,
    createdAt   datetime     not null,
    expiresAt   datetime     not null,
    confirmedAt datetime     null,
    user_id     int          not null,
    constraint confirmation_tokens_token_id_uindex
        unique (token_id),
    constraint confirmation_tokens_token_uindex
        unique (token),
    constraint confirmation_tokens_user_id_uindex
        unique (user_id),
    constraint confirmation_tokens_users_id_fk
        foreign key (user_id) references users (id)
);

alter table confirmation_tokens
    add primary key (token_id);

create table teams
(
    id           int auto_increment,
    name         varchar(30)          not null,
    owner_id     int                  not null,
    deleted_flag tinyint(1) default 0 not null,
    constraint table_name_id_uindex
        unique (id),
    constraint table_name_name_uindex
        unique (name),
    constraint table_name_users_user_id_fk
        foreign key (owner_id) references users (id)
);

alter table teams
    add primary key (id);

create table team_members
(
    team_id   int not null,
    member_id int not null,
    constraint team_members_teams_id_fk
        foreign key (team_id) references teams (id),
    constraint team_members_users_id_fk
        foreign key (member_id) references users (id)
);

create table work_items
(
    id              int auto_increment,
    title           varchar(80)          not null,
    description     varchar(21844)       not null,
    team_id         int                  not null,
    creator_id      int                  not null,
    reviewer_id     int                  not null,
    status_id       int                  not null,
    has_attachments tinyint(1) default 0 not null,
    deleted_flag    tinyint(1) default 0 not null,
    constraint work_items_id_uindex
        unique (id),
    constraint work_items_statuses_id_fk
        foreign key (status_id) references statuses (id),
    constraint work_items_teams_id_fk
        foreign key (team_id) references teams (id),
    constraint work_items_users_id_fk
        foreign key (creator_id) references users (id),
    constraint work_items_users_id_fk_2
        foreign key (reviewer_id) references users (id)
);

alter table work_items
    add primary key (id);

create table comments
(
    id      int auto_increment,
    content varchar(2000) not null,
    item_id int           not null,
    constraint comments_id_uindex
        unique (id),
    constraint comments_work_items_id_fk
        foreign key (item_id) references work_items (id)
);

alter table comments
    add primary key (id);

