USE `source-database`;

INSERT INTO `users` (id, username, password, email, phone_number, photo_url, enabled, deleted_flag)
VALUES (1, 'johnkata', 'Password.1', 'john.caster@gmail.com', '0892469384', 'johnkata.png', 1, 0),
       (2, 'hulkmaster88', 'Password.2', 'markman@yahoo.com', '1584839209', 'hulkmaster.jpg', 1, 0),
       (3, 'measley9', 'Password.3', 'steve.measley@ffco.com', '9580492123', 'measley.webp', 1, 0),
       (4, 'carter', 'Password.4', 'cartman25@outlook.com', '0014942834', 'carter.jpg', 1, 0),
       (5, 'steveG', 'Password.5', 'michelle.stevenson@ffco.com', '5849390394', 'steveG.jpg', 1, 0),
       (6, 'RoCar11', 'Password.6', 'roncard@gmail.com', '1120504837', 'rocar.jpg', 1, 0),
       (7, 'hessM', 'Password.7', 'mack_hess@gmail.com', '1542153218', 'mackhes.jpg', 1, 0),
       (8, 'MariamSH', 'Password.8', 'shannon_94@yahoo.com', '7789875642', 'mariam.jpg', 1, 0),
       (9, 'Cortess', 'Password.9', 'kennyCortess@gmail.com', '1221435754', 'cortess.jpg', 1, 0),
       (10, 'Dejefion', 'Password.10', 'dejefu@outlook.com', '7656784561', 'dejefion.png', 1, 0);

INSERT INTO `confirmation_tokens` (token_id, token, createdAt, expiresAt, confirmedAt, user_id)
VALUES (1, 'c5e34f81-890a-419c-acf3-6ef03d66abaa', '2021-11-11 15:53:55', '2021-11-11 16:08:55', '2021-11-11 15:54:11',
        1),
       (2, 'c5e34f81-890a-419c-acf3-6ef03d66abab', '2021-10-10 15:53:55', '2021-10-10 16:08:55', '2021-10-10 15:54:11',
        2),
       (3, 'c5e34f81-890a-419c-acf3-6ef03d66abac', '2021-9-12 15:53:55', '2021-9-12 16:08:55', '2021-9-12 15:54:11', 3),
       (4, 'c5e34f81-890a-419c-acf3-6ef03d66abad', '2021-8-13 15:53:55', '2021-8-13 16:08:55', '2021-8-13 15:54:11', 4),
       (5, 'c5e34f81-890a-419c-acf3-6ef03d66abae', '2021-7-13 15:53:55', '2021-7-13 16:08:55', '2021-7-13 15:54:11', 5),
       (6, 'c5e34f81-890a-419c-acf3-6ef03d66abaf', '2021-6-13 15:53:55', '2021-6-13 16:08:55', '2021-6-13 15:54:11', 6),
       (7, 'c5e34f81-890a-419c-acf3-6ef03d66abag', '2021-5-13 15:53:55', '2021-5-13 16:08:55', '2021-5-13 15:54:11', 7),
       (8, 'c5e34f81-890a-419c-acf3-6ef03d66abah', '2021-4-13 15:53:55', '2021-4-13 16:08:55', '2021-4-13 15:54:11', 8),
       (9, 'c5e34f81-890a-419c-acf3-6ef03d66abai', '2021-3-13 15:53:55', '2021-3-13 16:08:55', '2021-3-13 15:54:11', 9),
       (10, 'c5e34f81-890a-419c-acf3-6ef03d66abaj', '2021-2-13 15:53:55', '2021-2-13 16:08:55', '2021-2-13 15:54:11',
        10);

INSERT INTO `teams` (id, name, owner_id, deleted_flag)
VALUES (1, 'Team 1', 1, 0),
       (2, 'Team 2', 5, 0),
       (3, 'Team 3-1', 3, 0),
       (4, 'Team 3-2', 3, 0),
       (5, 'Team 3-3', 3, 0);

INSERT INTO `team_members` (team_id, member_id)
VALUES (1, 1),
       (1, 2),
       (1, 4),
       (2, 5),
       (2, 6),
       (2, 3),
       (2, 7),
       (3, 8),
       (3, 9),
       (3, 3),
       (3, 1),
       (3, 2),
       (4, 3),
       (4, 5),
       (5, 4),
       (5, 3);

INSERT INTO `statuses` (id, name)
VALUES (1, 'Pending'),
       (2, 'Under Review'),
       (3, 'Change Requested'),
       (4, 'Accepted'),
       (5, 'Rejected');

INSERT INTO `work_items` (id, title, description, team_id, creator_id, reviewer_id, status_id, deleted_flag)
VALUES (1, '"Beyond the veil" sci-fi draft review', 'Review of the draft for "Beyond the veil"', 1, 1, 2, 1, 0),
       (2, 'Review of academic research paper on "Nicotinamide riboside"',
        'Review and evaluation of the findings in a scientific paper studying the effects of Nicotinamide riboside on the human body',
        2, 5, 7, 3, 0),
       (3, 'Refactor the buttons on the home page',
        'Remove the old style buttons and replace them with the updated bootstrap package buttons from April 2021', 3,
        3, 9, 1, 0),
       (4, 'Change the layout of data tables on the home page',
        'Move the data tables around in a way that is coherent and logical', 3, 3, 8, 2, 0),
       (5, 'Update the header and footer', 'Update the header and footer with the new company logo and trademark', 3, 3,
        6, 2, 0),
       (6, 'Update the REST API endpoints to match the new structure',
        'Update the endpoints and make them match the new API structure (update the unit tests too!)', 4, 3, 5, 4, 0),
       (7, 'Finetune the vocals in this draft',
        'Go over the vocals and balance them where necessary; clear any distortion present and flesh out the EQ', 5, 3,
        4, 5, 0),
       (8, 'Mix the draft', 'Go over the draft and mix it properly so it is ready for mastering', 5, 3, 4, 5, 0),
       (9, 'Master the draft', 'Go over the draft and master it entirely so it\'s ready for production', 5, 3, 4, 5, 0),
       (10, 'Finalize the unit tests',
        'Once all other changes have been completed, update the unit tests and make sure they pass', 3, 3, 2, 1, 0);

INSERT INTO `comments` (id, content, item_id)
VALUES (1, 'Please update the sources, as they seem to be missing a few entries.', 2),
       (2, 'Need to remodel the APIs bound to these buttons as well.', 3),
       (3, 'Also update the icons in the sidebar with more suitable ones that match the theme.', 3),
       (4,
        'The data supplied to the table about the monthly sales seems to be referring to 2 months ago and not last month. Please update.',
        4),
       (5, 'The format of these tables is a bit confusing, maybe it will be best to explore some other designs?', 4),
       (6,
        'We need to figure out a way to place those tables so they are not too obtrusive and irritating. Opening the home page to be hit with 8 tables straight away might be offputting to some people.',
        4);