package com.telerikacademy.reviewit.services;

import com.telerikacademy.reviewit.exceptions.DuplicateEntityException;
import com.telerikacademy.reviewit.exceptions.ForbiddenOperationException;
import com.telerikacademy.reviewit.exceptions.InvalidUserInputException;
import com.telerikacademy.reviewit.models.Comment;
import com.telerikacademy.reviewit.models.Item;
import com.telerikacademy.reviewit.models.User;
import com.telerikacademy.reviewit.repositories.CRUDRepository;
import com.telerikacademy.reviewit.repositories.contracts.ItemRepository;
import com.telerikacademy.reviewit.services.contracts.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.Optional;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import static com.telerikacademy.reviewit.services.ServiceConstants.*;

@Service
public class ItemServiceImpl implements ItemService {

    private final ItemRepository itemRepository;
    private final CRUDRepository<Comment> commentRepository;

    @Autowired
    public ItemServiceImpl(ItemRepository itemRepository, CRUDRepository<Comment> commentRepository) {
        this.itemRepository = itemRepository;
        this.commentRepository = commentRepository;
        commentRepository.setClassType(Comment.class);
    }


    @Override
    public List<Item> getAll() {
        return itemRepository.getAll();
    }

    @Override
    public List<Item> filter(Optional<String> status, Optional<String> reviewer, Optional<String> creator, Optional<String> title, Optional<Integer> teamID, Optional<String> searchAll, Optional<String> sortBy) {
        return itemRepository.filter(status, reviewer, creator, title, teamID, searchAll, sortBy);
    }

    @Override
    public Item getByID(int id) {
        return itemRepository.getByID(id);
    }

    @Override
    public void create(Item item) {

        if (titleExists(item)) {
            throw new DuplicateEntityException("Item", "title", item.getTitle());
        }

        if (hasTheSameUserForCreatorAndReviewer(item)) {
            throw new InvalidUserInputException(CREATOR_AND_REVIEWER_DUPLICATE_ERR_MSG);
        }

        if (isItemTeamDifferentFromReviewerTeam(item)) {
            throw new InvalidUserInputException(String.format(ITEM_TEAM_DIFFERENT_FROM_REVIEWER_TEAM_ERR, item.getTeam().getName()));
        }

        itemRepository.create(item);
    }

    @Override
    public void update(Item item, User user) {

        Item itemToUpdate = itemRepository.getByID(item.getId());

        if (hasChangeInOtherThanStatus(itemToUpdate, item) && isNotCreator(itemToUpdate, user)) {
            throw new ForbiddenOperationException(String.format(FORBIDDEN_OPERATION_ERR_MSG, "modify"));
        }

        if (hasChangeInStatus(item, itemToUpdate) && isNotReviewer(user, itemToUpdate)) {
            throw new ForbiddenOperationException(USER_IS_NOT_REVIEWER_ERR_MSG);
        }

        if (titleExists(item, itemToUpdate.getId())) {
            throw new DuplicateEntityException("Item", "name", item.getTitle());
        }

        if (hasChangeInTeam(item, itemToUpdate)) {
            throw new ForbiddenOperationException(
                    String.format(ITEM_ALREADY_IN_TEAM_ERR_MSG,
                            itemToUpdate.getId(),
                            itemToUpdate.getTeam().getName()));
        }

        if (hasTheSameUserForCreatorAndReviewer(item)) {
            throw new InvalidUserInputException(CREATOR_AND_REVIEWER_DUPLICATE_ERR_MSG);
        }

        if (isItemTeamDifferentFromReviewerTeam(item)) {
            throw new InvalidUserInputException(String.format(ITEM_TEAM_DIFFERENT_FROM_REVIEWER_TEAM_ERR, item.getTeam().getName()));
        }

        if (isStatusChangedToRejectedOrChangeRequested(item, itemToUpdate)) {
            if (item.getComment().length() == 0) {
                throw new InvalidUserInputException(EMPTY_COMMENT_ERR_MSG);
            }
            Comment comment = new Comment();
            comment.setContent(item.getComment());
            comment.setItem(item);
            commentRepository.create(comment);
        }

        itemRepository.update(item);
    }

    @Override
    public void delete(Item item, User user) {

        if (item.getCreator().getID() != user.getID()) {
            throw new ForbiddenOperationException(String.format(FORBIDDEN_OPERATION_ERR_MSG, "delete"));
        }

        itemRepository.delete(item);
    }

    public void saveFile(String uploadDir, String fileName, MultipartFile attachment) throws IOException {
        Path uploadPath = Paths.get(uploadDir);

        if (!Files.exists(uploadPath)) {
            Files.createDirectories(uploadPath);
        }

        try (InputStream inputStream = attachment.getInputStream()) {
            Path filePath = uploadPath.resolve(fileName);
            Files.copy(inputStream, filePath, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            throw new RuntimeException("Could not upload file: " + fileName);
        }
    }

    public void zipAttachments(String sourceDirPath, String zipFilePath, String zipName) throws IOException {
        Path p = Files.createFile(Paths.get(zipFilePath));
        try (ZipOutputStream zs = new ZipOutputStream(Files.newOutputStream(p))) {
            Path pp = Paths.get(sourceDirPath);
            Files.walk(pp)
                    .filter(path -> !Files.isDirectory(path))
                    .filter(path -> !path.getFileName().toString().equals(zipName))
                    .forEach(path -> {
                        ZipEntry zipEntry = new ZipEntry(pp.relativize(path).toString());
                        try {
                            zs.putNextEntry(zipEntry);
                            Files.copy(path, zs);
                            zs.closeEntry();
                        } catch (IOException e) {
                            throw new RuntimeException("");
                        }
                    });
        }
    }

    private boolean isNotCreator(Item itemToUpdate, User user) {
        return itemToUpdate.getCreator().getID() != user.getID();
    }

    private boolean hasChangeInOtherThanStatus(Item itemToUpdate, Item item) {
        return !itemToUpdate.getTitle().equalsIgnoreCase(item.getTitle()) ||
                !itemToUpdate.getDescription().equalsIgnoreCase(item.getDescription()) ||
                itemToUpdate.getTeam().getId() != item.getTeam().getId() ||
                itemToUpdate.getCreator().getID() != item.getCreator().getID() ||
                itemToUpdate.getReviewer().getID() != item.getReviewer().getID();
    }

    private boolean isStatusChangedToRejectedOrChangeRequested(Item item, Item itemToUpdate) {
        return !itemToUpdate.getStatus().getName().equals(item.getStatus().getName())
                && (item.getStatus().getName().equalsIgnoreCase("Rejected") ||
                item.getStatus().getName().equalsIgnoreCase("Change Requested"));
    }

    private boolean hasChangeInTeam(Item item, Item itemToUpdate) {
        return itemToUpdate.getTeam().getId() != item.getTeam().getId();
    }

    private boolean titleExists(Item item) {
        return getAll().stream().anyMatch(item1 -> item1.getTitle().equalsIgnoreCase(item.getTitle()));
    }

    private boolean titleExists(Item item, int itemToUpdateID) {
        Optional<Item> duplicateItem = getAll().stream().filter(item1 -> item1.getTitle().equalsIgnoreCase(item.getTitle())).findFirst();
        return duplicateItem.isPresent() && duplicateItem.get().getId() != itemToUpdateID;
    }

    private boolean hasTheSameUserForCreatorAndReviewer(Item item) {
        return item.getCreator().getID() == item.getReviewer().getID();
    }

    private boolean isItemTeamDifferentFromReviewerTeam(Item item) {
        return item.getTeam().getMembers().stream().noneMatch(member -> member.equals(item.getReviewer()));
    }

    private boolean hasChangeInStatus(Item item, Item itemToUpdate) {
        return !item.getStatus().getName().equalsIgnoreCase(itemToUpdate.getStatus().getName());
    }

    private boolean isNotReviewer(User user, Item itemToUpdate) {
        return itemToUpdate.getReviewer().getID() != user.getID();
    }

}
