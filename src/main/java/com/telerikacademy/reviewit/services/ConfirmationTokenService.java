package com.telerikacademy.reviewit.services;

import com.telerikacademy.reviewit.models.ConfirmationToken;
import com.telerikacademy.reviewit.models.User;
import com.telerikacademy.reviewit.repositories.contracts.ConfirmationTokenRepository;
import com.telerikacademy.reviewit.repositories.contracts.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class ConfirmationTokenService {

    private final ConfirmationTokenRepository tokenRepository;
    private final UserRepository<User> userRepository;

    @Autowired
    public ConfirmationTokenService(ConfirmationTokenRepository tokenRepository, UserRepository<User> userRepository) {
        this.tokenRepository = tokenRepository;
        this.userRepository = userRepository;
    }

    public void createToken(ConfirmationToken token) {
        tokenRepository.create(token);
    }


    public void confirmToken(String token) {
        ConfirmationToken confirmationToken = tokenRepository
                .findByToken(token)
                .orElseThrow(() -> new IllegalStateException("Token not found!"));

        if (confirmationToken.getConfirmedAt() != null) {
            throw new IllegalStateException("User already confirmed");
        }

        LocalDateTime expiredAt = confirmationToken.getExpiresAt();

        if (expiredAt.isBefore(LocalDateTime.now())) {
            throw new IllegalStateException("Token expired");
        }

        confirmationToken.setConfirmedAt(LocalDateTime.now());

        tokenRepository.update(confirmationToken);

        User user = confirmationToken.getUser();
        user.setEnabled(true);

        userRepository.update(user);
    }
}
