package com.telerikacademy.reviewit.services;

public class ServiceConstants {

    // Team Service
    public static final String MEMBER_IN_TEAM_ERR = "%s is already in your team";
    public static final String MEMBER_NOT_IN_TEAM_ERR = "User with ID %d and username '%s' is not in team: %s";
    public static final String TEAM_NAME_TAKEN = "Team name taken, please choose a different one!";
    public static final String TEAM_STILL_HAS_MEMBERS = "Team still has members, please delete those first!";
    public static final String USER_IS_NOT_OWNER_ERR_MSG = "Only the owner is allowed to make changes to the team.";
    public static final String TEAM_OWNER_CHANGE_ERR_MSG = "Team owner can not be changed!";

    // Item Service
    public static final String EMPTY_COMMENT_ERR_MSG = "You must leave a new comment!";
    public static final String ITEM_ALREADY_IN_TEAM_ERR_MSG = "Item with ID: %d is already assigned to Team: %s";
    public static final String CREATOR_AND_REVIEWER_DUPLICATE_ERR_MSG = "Creator and Reviewer can not be the same user!";
    public static final String ITEM_TEAM_DIFFERENT_FROM_REVIEWER_TEAM_ERR = "Reviewer must be from Team: %s";
    public static final String FORBIDDEN_OPERATION_ERR_MSG = "Only the creator can %s the item!";
    public static final String USER_IS_NOT_REVIEWER_ERR_MSG = "Only the reviewer can change the status";

    // User Service
    public static final String UPDATE_USER_ERR = "Only the account owner can update it!";
    public static final String DELETE_USER_REVIEWER_CREATOR_ERR = "User still assigned as creator or reviewer to an item!";
    public static final String DELETE_USER_TEAM_OWNER_ERR = "User is owner of a team, please delete the team first!";
}
