package com.telerikacademy.reviewit.services;

import com.telerikacademy.reviewit.exceptions.DuplicateEntityException;
import com.telerikacademy.reviewit.exceptions.ForbiddenOperationException;
import com.telerikacademy.reviewit.models.Item;
import com.telerikacademy.reviewit.models.Team;
import com.telerikacademy.reviewit.models.User;
import com.telerikacademy.reviewit.repositories.contracts.Repository;
import com.telerikacademy.reviewit.services.contracts.ItemService;
import com.telerikacademy.reviewit.services.contracts.TeamService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.telerikacademy.reviewit.services.ServiceConstants.*;

@Service
public class TeamServiceImpl implements TeamService {


    private final Repository<Team> teamRepository;
    private final ItemService itemService;

    public TeamServiceImpl(Repository<Team> teamRepository, ItemService itemService) {
        this.teamRepository = teamRepository;
        this.itemService = itemService;
        teamRepository.setClassType(Team.class);
    }

    @Override
    public List<Team> getAll() {
        return teamRepository.getAll();
    }

    @Override
    public Team getByID(int id) {
        return teamRepository.getByID(id);
    }

    @Override
    public void create(Team team) {

        if (nameExists(team)) {
            throw new DuplicateEntityException(TEAM_NAME_TAKEN);
        }

        teamRepository.create(team);
        addMember(team, team.getOwner(), team.getOwner());
    }

    @Override
    public void update(Team team, User user) {

        Team teamToUpdate = teamRepository.getByID(team.getId());

        if (teamToUpdate.getOwner().getID() != team.getOwner().getID()) {
            throw new ForbiddenOperationException(TEAM_OWNER_CHANGE_ERR_MSG);
        }

        if (team.getOwner().getID() != user.getID()) {
            throw new ForbiddenOperationException(USER_IS_NOT_OWNER_ERR_MSG);
        }

        if (nameExists(team, team.getId())) {
            throw new DuplicateEntityException(TEAM_NAME_TAKEN);
        }

        teamRepository.update(team);
    }

    @Override
    public void delete(Team team, User user) {

        if (team.getOwner().getID() != user.getID()) {
            throw new ForbiddenOperationException(USER_IS_NOT_OWNER_ERR_MSG);
        }

        if (!listMembers(team).isEmpty()) {
            throw new ForbiddenOperationException(TEAM_STILL_HAS_MEMBERS);
        }
        teamRepository.delete(team);
    }

    @Override
    public void addMember(Team team, User member, User user) {

        if (team.getOwner().getID() != user.getID()) {
            throw new ForbiddenOperationException(USER_IS_NOT_OWNER_ERR_MSG);
        }

        if (team.getMembers().add(member)) {
            teamRepository.update(team);
            return;
        }

        throw new DuplicateEntityException(String.format(MEMBER_IN_TEAM_ERR, member.getUsername()));
    }

    @Override
    public void removeMember(Team team, User member) {


        if (team.getMembers().remove(member)) {
            teamRepository.update(team);
            return;
        }

        throw new ForbiddenOperationException(String.format(MEMBER_NOT_IN_TEAM_ERR, member.getID(), member.getUsername(), team.getName()));
    }

    @Override
    public List<User> listMembers(Team team) {
        return new ArrayList<>(team.getMembers());
    }

    @Override
    public List<Item> listItems(Team team) {
        return new ArrayList<>(itemService.filter(
                Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.of(team.getId()),
                Optional.empty(),
                Optional.empty()));
    }

    private boolean nameExists(Team team) {
        return teamRepository.getAll().stream().anyMatch(team1 -> team1.getName().equals(team.getName()));
    }

    private boolean nameExists(Team team, int teamToUpdateID) {
        Optional<Team> duplicateTeam = getAll().stream().filter(team1 -> team1.getName().equalsIgnoreCase(team.getName())).findFirst();
        return duplicateTeam.isPresent() && duplicateTeam.get().getId() != teamToUpdateID;
    }

}
