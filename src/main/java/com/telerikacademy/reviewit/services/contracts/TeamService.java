package com.telerikacademy.reviewit.services.contracts;

import com.telerikacademy.reviewit.models.Item;
import com.telerikacademy.reviewit.models.Team;
import com.telerikacademy.reviewit.models.User;

import java.util.List;

public interface TeamService extends CRUDService<Team> {

    void addMember(Team team, User member, User user);

    void removeMember(Team team, User member);

    List<User> listMembers(Team team);

    List<Item> listItems(Team team);
}
