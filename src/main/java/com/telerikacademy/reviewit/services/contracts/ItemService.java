package com.telerikacademy.reviewit.services.contracts;

import com.telerikacademy.reviewit.models.Item;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public interface ItemService extends CRUDService<Item> {

    List<Item> filter(Optional<String> status, Optional<String> reviewer, Optional<String> creator, Optional<String> title, Optional<Integer> teamID, Optional<String> searchAll, Optional<String> sortBy);

    void saveFile(String uploadDir, String fileName, MultipartFile attachment) throws IOException;

    void zipAttachments(String sourceDirPath, String zipFilePath, String zipName) throws IOException;
}

