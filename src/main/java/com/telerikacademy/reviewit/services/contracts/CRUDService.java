package com.telerikacademy.reviewit.services.contracts;

import com.telerikacademy.reviewit.models.User;

import java.io.Serializable;
import java.util.List;

public interface CRUDService<T extends Serializable> {

    List<T> getAll();

    T getByID(int id);

    void create(T entity);

    void update(T entity, User user);

    void delete(T entity, User user);

}
