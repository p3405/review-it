package com.telerikacademy.reviewit.services.contracts;

import com.telerikacademy.reviewit.models.User;

import java.util.List;
import java.util.Optional;

public interface UserService extends CRUDService<User> {

    List<User> filter(Optional<String> username, Optional<String> email, Optional<String> phone, Optional<String> searchAll, Optional<String> sortBy);
}
