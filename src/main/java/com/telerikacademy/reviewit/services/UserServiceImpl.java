package com.telerikacademy.reviewit.services;

import com.telerikacademy.reviewit.exceptions.DuplicateEntityException;
import com.telerikacademy.reviewit.exceptions.ForbiddenOperationException;
import com.telerikacademy.reviewit.exceptions.InvalidUserInputException;
import com.telerikacademy.reviewit.models.ConfirmationToken;
import com.telerikacademy.reviewit.models.User;
import com.telerikacademy.reviewit.repositories.contracts.UserRepository;
import com.telerikacademy.reviewit.services.contracts.ItemService;
import com.telerikacademy.reviewit.services.contracts.TeamService;
import com.telerikacademy.reviewit.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static com.telerikacademy.reviewit.services.ServiceConstants.*;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository<User> userRepository;
    private final ItemService itemService;
    private final TeamService teamService;
    private final ConfirmationTokenService tokenService;
    private final EmailService emailService;

    @Autowired
    public UserServiceImpl(UserRepository<User> userRepository,
                           ItemService itemService,
                           TeamService teamService, ConfirmationTokenService tokenService, EmailService emailService) {
        this.userRepository = userRepository;
        this.itemService = itemService;
        this.teamService = teamService;
        this.tokenService = tokenService;
        this.emailService = emailService;
    }

    @Override
    public List<User> getAll() {
        return userRepository.getAll();
    }

    @Override
    public User getByID(int id) {
        return userRepository.getByID(id);
    }

    @Override
    public void create(User user) {
        checkDuplicateField(user, "username");
        checkDuplicateField(user, "email");
        checkDuplicateField(user, "phone number");

        userRepository.create(user);

        String token = UUID.randomUUID().toString();

        ConfirmationToken confirmationToken = new ConfirmationToken(
                token,
                LocalDateTime.now(),
                LocalDateTime.now().plusMinutes(60),
                user);

        tokenService.createToken(confirmationToken);

        String link = "http://localhost:8080/auth/register/confirm?token=" + confirmationToken.getToken();
        emailService.send(user.getEmail(), buildEmail(user.getUsername(), link));

    }

    @Override
    public void update(User userToUpdate, User user) {
        if (userToUpdate.getID() != user.getID()) {
            throw new ForbiddenOperationException(UPDATE_USER_ERR);
        }

        if (duplicateUsername(user)) {
            throw new DuplicateEntityException("User", "username", user.getUsername());
        }

        if (duplicateEmail(user)) {
            throw new DuplicateEntityException("User", "email", user.getEmail());
        }

        if (duplicatePhone(user)) {
            throw new DuplicateEntityException("User", "phone number", user.getPhoneNumber());
        }

        userRepository.update(user);
    }

    @Override
    public void delete(User userToDelete, User user) {
        if (itemService.getAll()
                .stream()
                .anyMatch(item -> item.getCreator().getID() == userToDelete.getID() || item.getReviewer().getID() == userToDelete.getID())) {
            throw new ForbiddenOperationException(DELETE_USER_REVIEWER_CREATOR_ERR);
        }

        if (teamService.getAll().stream().anyMatch(team -> team.getOwner().getID() == userToDelete.getID())) {
            throw new ForbiddenOperationException(DELETE_USER_TEAM_OWNER_ERR);
        }

        userRepository.delete(user);
    }

    @Override
    public List<User> filter(Optional<String> username, Optional<String> email, Optional<String> phone, Optional<String> searchAll, Optional<String> sortBy) {
        return userRepository.filter(username, email, phone, searchAll, sortBy);
    }

    private boolean duplicateUsername(User user) {
        Optional<User> duplicateUser = userRepository.getAll().stream().filter(user1 -> user1.getUsername().equals(user.getUsername())).findFirst();
        return duplicateUser.isPresent() && duplicateUser.get().getID() != user.getID();
    }

    private boolean duplicateEmail(User user) {
        Optional<User> duplicateUser = userRepository.getAll().stream().filter(user1 -> user1.getEmail().equals(user.getEmail())).findFirst();
        return duplicateUser.isPresent() && duplicateUser.get().getID() != user.getID();
    }

    private boolean duplicatePhone(User user) {
        Optional<User> duplicateUser = userRepository.getAll().stream().filter(user1 -> user1.getPhoneNumber().equals(user.getPhoneNumber())).findFirst();
        return duplicateUser.isPresent() && duplicateUser.get().getID() != user.getID();
    }

    private void checkDuplicateField(User user, String field) {
        boolean checkUsername = field.equalsIgnoreCase("username");
        boolean checkEmail = field.equalsIgnoreCase("email");

        if (userRepository.getAll()
                .stream()
                .map(checkUsername ? User::getUsername : checkEmail ? User::getEmail : User::getPhoneNumber)
                .anyMatch(
                        checkUsername ? username -> username.equals(user.getUsername()) :
                                checkEmail ? email -> email.equals(user.getEmail()) :
                                        phoneNum -> phoneNum.equals(user.getPhoneNumber()))
        ) {
            throw new DuplicateEntityException("User", field,
                    checkUsername ? user.getUsername() : checkEmail ? user.getEmail() : user.getPhoneNumber());
        }
    }

    public void validatePasswordChange(User user, String currentPassword, String newPassword, String newPasswordConfirm) {
        if (!currentPassword.equals(user.getPassword())) {
            throw new InvalidUserInputException("Current password incorrect.");
        }

        if (!newPassword.equals(newPasswordConfirm)) {
            throw new InvalidUserInputException("Passwords don't match.");
        }
    }

    private String buildEmail(String name, String link) {
        return "<div style=\"font-family:Helvetica,Arial,sans-serif;font-size:16px;margin:0;color:#0b0c0c\">\n" +
                "\n" +
                "<span style=\"display:none;font-size:1px;color:#fff;max-height:0\"></span>\n" +
                "\n" +
                "  <table role=\"presentation\" width=\"100%\" style=\"border-collapse:collapse;min-width:100%;width:100%!important\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\n" +
                "    <tbody><tr>\n" +
                "      <td width=\"100%\" height=\"53\" bgcolor=\"#0b0c0c\">\n" +
                "        \n" +
                "        <table role=\"presentation\" width=\"100%\" style=\"border-collapse:collapse;max-width:580px\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\">\n" +
                "          <tbody><tr>\n" +
                "            <td width=\"70\" bgcolor=\"#0b0c0c\" valign=\"middle\">\n" +
                "                <table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse\">\n" +
                "                  <tbody><tr>\n" +
                "                    <td style=\"padding-left:10px\">\n" +
                "                  \n" +
                "                    </td>\n" +
                "                    <td style=\"font-size:28px;line-height:1.315789474;Margin-top:4px;padding-left:10px\">\n" +
                "                      <span style=\"font-family:Helvetica,Arial,sans-serif;font-weight:700;color:#ffffff;text-decoration:none;vertical-align:top;display:inline-block\">Confirm your email</span>\n" +
                "                    </td>\n" +
                "                  </tr>\n" +
                "                </tbody></table>\n" +
                "              </a>\n" +
                "            </td>\n" +
                "          </tr>\n" +
                "        </tbody></table>\n" +
                "        \n" +
                "      </td>\n" +
                "    </tr>\n" +
                "  </tbody></table>\n" +
                "  <table role=\"presentation\" class=\"m_-6186904992287805515content\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse;max-width:580px;width:100%!important\" width=\"100%\">\n" +
                "    <tbody><tr>\n" +
                "      <td width=\"10\" height=\"10\" valign=\"middle\"></td>\n" +
                "      <td>\n" +
                "        \n" +
                "                <table role=\"presentation\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse\">\n" +
                "                  <tbody><tr>\n" +
                "                    <td bgcolor=\"#1D70B8\" width=\"100%\" height=\"10\"></td>\n" +
                "                  </tr>\n" +
                "                </tbody></table>\n" +
                "        \n" +
                "      </td>\n" +
                "      <td width=\"10\" valign=\"middle\" height=\"10\"></td>\n" +
                "    </tr>\n" +
                "  </tbody></table>\n" +
                "\n" +
                "\n" +
                "\n" +
                "  <table role=\"presentation\" class=\"m_-6186904992287805515content\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse;max-width:580px;width:100%!important\" width=\"100%\">\n" +
                "    <tbody><tr>\n" +
                "      <td height=\"30\"><br></td>\n" +
                "    </tr>\n" +
                "    <tr>\n" +
                "      <td width=\"10\" valign=\"middle\"><br></td>\n" +
                "      <td style=\"font-family:Helvetica,Arial,sans-serif;font-size:19px;line-height:1.315789474;max-width:560px\">\n" +
                "        \n" +
                "            <p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;color:#0b0c0c\">Hi " + name + ",</p><p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;color:#0b0c0c\"> Thank you for registering. Please click on the below link to activate your account: </p><blockquote style=\"Margin:0 0 20px 0;border-left:10px solid #b1b4b6;padding:15px 0 0.1px 15px;font-size:19px;line-height:25px\"><p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;color:#0b0c0c\"> <a href=\"" + link + "\">Activate Now</a> </p></blockquote>\n Link will expire in 15 minutes. <p>See you soon</p>" +
                "        \n" +
                "      </td>\n" +
                "      <td width=\"10\" valign=\"middle\"><br></td>\n" +
                "    </tr>\n" +
                "    <tr>\n" +
                "      <td height=\"30\"><br></td>\n" +
                "    </tr>\n" +
                "  </tbody></table><div class=\"yj6qo\"></div><div class=\"adL\">\n" +
                "\n" +
                "</div></div>";
    }

}
