package com.telerikacademy.reviewit.utils;

import com.telerikacademy.reviewit.models.Item;
import com.telerikacademy.reviewit.models.Status;
import com.telerikacademy.reviewit.models.User;
import com.telerikacademy.reviewit.models.inputDTOs.ItemDTO;
import com.telerikacademy.reviewit.repositories.CRUDRepository;
import com.telerikacademy.reviewit.repositories.contracts.UserRepository;
import com.telerikacademy.reviewit.services.contracts.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ItemModelMapper {

    public static final int INITIAL_PENDING_STATUS_ID = 1;

    private final CRUDRepository<Status> statusRepository;
    private final UserRepository<User> userService;
    private final TeamService teamService;

    @Autowired
    public ItemModelMapper(CRUDRepository<Status> statusRepository, UserRepository<User> userService, TeamService teamService) {
        this.statusRepository = statusRepository;
        statusRepository.setClassType(Status.class);
        this.userService = userService;
        this.teamService = teamService;
    }

    public Item itemFromDTO(ItemDTO itemDTO) {
        Item item = new Item();
        item.setStatus(statusRepository.getByID(INITIAL_PENDING_STATUS_ID));
        return itemFromDTO(itemDTO, item);
    }

    public Item itemFromDTO(ItemDTO itemDTO, Item item) {

        if (item.getTitle() != null) {
            item.setStatus(statusRepository.getByID(itemDTO.getStatusID()));
        }

        item.setTitle(itemDTO.getTitle());
        item.setDescription(itemDTO.getDescription());
        item.setTeam(teamService.getByID(itemDTO.getTeamID()));
        item.setCreator(userService.getByID(itemDTO.getCreatorID()));
        item.setReviewer(userService.getByID(itemDTO.getReviewerID()));
        item.setComment(itemDTO.getComment());

        if (itemDTO.getAttachment() != null && itemDTO.getAttachment().getOriginalFilename().length() > 0) {
            item.setHasAttachments(true);
        }

        return item;
    }

    public ItemDTO itemToDTO(Item item) {
        ItemDTO output = new ItemDTO();
        output.setTitle(item.getTitle());
        output.setDescription(item.getDescription());
        output.setCreatorID(item.getCreator().getID());
        output.setReviewerID(item.getReviewer().getID());
        output.setTeamID(item.getTeam().getId());
        output.setStatusID(item.getStatus().getId());
        return output;
    }
}
