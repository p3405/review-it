package com.telerikacademy.reviewit.utils;

import com.telerikacademy.reviewit.models.Team;
import com.telerikacademy.reviewit.models.inputDTOs.TeamDTO;
import com.telerikacademy.reviewit.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;

@Component
public class TeamModelMapper {

    private final UserService userService;


    @Autowired
    public TeamModelMapper(UserService userService) {
        this.userService = userService;
    }

    public Team teamFromDTO(TeamDTO teamDTO) {
        Team team = new Team();
        return teamFromDTO(teamDTO, team);
    }

    public Team teamFromDTO(TeamDTO teamDTO, Team team) {

        team.setName(teamDTO.getName());
        team.setOwner(userService.getByID(teamDTO.getOwnerID()));
        team.setMembers(new HashSet<>());

        return team;
    }
}
