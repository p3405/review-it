package com.telerikacademy.reviewit.utils;

import com.telerikacademy.reviewit.exceptions.InvalidUserInputException;
import com.telerikacademy.reviewit.models.User;
import com.telerikacademy.reviewit.models.inputDTOs.RegisterDTO;
import com.telerikacademy.reviewit.models.inputDTOs.UserChangePasswordDTO;
import com.telerikacademy.reviewit.models.inputDTOs.UserDTO;
import com.telerikacademy.reviewit.models.outputDTOs.DisplayUserDTO;
import com.telerikacademy.reviewit.services.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserModelMapper {

    private final UserServiceImpl userService;

    @Autowired
    public UserModelMapper(UserServiceImpl userService) {
        this.userService = userService;
    }

    public DisplayUserDTO displayUser(User user) {
        DisplayUserDTO displayUserDTO = new DisplayUserDTO();

        displayUserDTO.setID(user.getID());
        displayUserDTO.setUsername(user.getUsername());
        return displayUserDTO;
    }

    public User userFromDTO(UserDTO userDTO) {
        User user = new User();
        return userFromDTO(user, userDTO);
    }

    public User userFromDTO(User user, UserDTO userDTO) {
        user.setUsername(userDTO.getUsername());
        user.setPassword(userDTO.getPassword());
        user.setEmail(userDTO.getEmail());
        user.setPhoneNumber(userDTO.getPhoneNum());

        return user;
    }

    public User userFromDTO(User user, UserDTO userDTO, UserChangePasswordDTO userChangePasswordDTO) {

        String currentPassword = userChangePasswordDTO.getOldPassword();
        String newPassword = userChangePasswordDTO.getNewPassword();
        String newPasswordConfirm = userChangePasswordDTO.getConfirmNewPassword();
        userService.validatePasswordChange(user, currentPassword, newPassword, newPasswordConfirm);

        userDTO.setUsername(user.getUsername());
        userDTO.setPassword(newPassword);
        userDTO.setEmail(user.getEmail());
        userDTO.setPhoneNum(user.getPhoneNumber());

        return userFromDTO(user, userDTO);
    }

    public UserDTO userToDTO(User user) {
        UserDTO userDTO = new UserDTO();

        userDTO.setUsername(user.getUsername());
        userDTO.setPassword(user.getPassword());
        userDTO.setEmail(user.getEmail());
        userDTO.setPhoneNum(user.getPhoneNumber());

        return userDTO;
    }

    public User userFromRegisterDTO(RegisterDTO registerDTO) {
        User user = new User();

        if (!registerDTO.getPassword().equals(registerDTO.getPasswordConfirm())) {
            throw new InvalidUserInputException("Passwords don't match.");
        }

        user.setUsername(registerDTO.getUsername());
        user.setPassword(registerDTO.getPassword());
        user.setEmail(registerDTO.getEmail());
        user.setPhoneNumber(registerDTO.getPhoneNum());

        return user;
    }


}
