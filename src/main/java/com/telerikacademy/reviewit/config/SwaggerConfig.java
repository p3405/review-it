package com.telerikacademy.reviewit.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.Collections;

@Configuration
public class SwaggerConfig {

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.telerikacademy.reviewit"))
                .build()
                .apiInfo(apiDetails());
    }

    private ApiInfo apiDetails() {
        return new ApiInfo(
                "ReviewIT",
                "Official API for Team-2 Project",
                "1.0",
                "https://gitlab.com/p3405/review-it",
                new Contact("Team2-Java33", "localhost:8080/swagger-ui.html", "Team2TelerikAcademy@gmail.com"),
                "Api License",
                "localhost:8080/swagger-ui.html",
                Collections.emptyList());
    }


}
