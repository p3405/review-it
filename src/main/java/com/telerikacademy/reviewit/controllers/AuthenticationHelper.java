package com.telerikacademy.reviewit.controllers;

import com.telerikacademy.reviewit.exceptions.AuthenticationFailureException;
import com.telerikacademy.reviewit.exceptions.EntityNotFoundException;
import com.telerikacademy.reviewit.models.User;
import com.telerikacademy.reviewit.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Optional;

@Component
public class AuthenticationHelper {

    public static final String AUTHORIZATION_HEADER_NAME = "Authorization";
    public static final String UNAUTHORIZED_ERR_MSG = "The requested resource requires authentication.";
    public static final String AUTHENTICATION_FAILURE_MESSAGE = "Wrong username or password.";
    public static final String NO_USER_LOGGED_IN = "No user logged in.";

    private final UserService userService;

    @Autowired
    public AuthenticationHelper(UserService userService) {
        this.userService = userService;
    }

    public User tryGetUser(HttpHeaders headers) {
        if (!headers.containsKey(AUTHORIZATION_HEADER_NAME)) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, UNAUTHORIZED_ERR_MSG);
        }

        try {
            String username = headers.getFirst(AUTHORIZATION_HEADER_NAME);
            return userService.filter((username != null ? Optional.of(username) : Optional.empty()), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty()).get(0);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Invalid username.");
        }
    }

    public User tryGetUser(HttpSession session) {
        String currentUser = (String) session.getAttribute("currentUser");

        if (currentUser == null) {
            throw new AuthenticationFailureException(NO_USER_LOGGED_IN);
        }

        return userService.filter(Optional.of(currentUser), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty()).get(0);
    }


    public void verifyAuthentication(String username, String password) {
        try {
            List<User> user = userService.filter(Optional.of(username), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty());
            if (user.size() == 0 || !user.get(0).getPassword().equals(password)) {
                throw new AuthenticationFailureException(AUTHENTICATION_FAILURE_MESSAGE);
            }
        } catch (EntityNotFoundException e) {
            throw new AuthenticationFailureException(AUTHENTICATION_FAILURE_MESSAGE);
        }
    }
}
