package com.telerikacademy.reviewit.controllers.rest;

import com.telerikacademy.reviewit.controllers.AuthenticationHelper;
import com.telerikacademy.reviewit.exceptions.*;
import com.telerikacademy.reviewit.models.Item;
import com.telerikacademy.reviewit.models.User;
import com.telerikacademy.reviewit.models.inputDTOs.ItemDTO;
import com.telerikacademy.reviewit.services.contracts.ItemService;
import com.telerikacademy.reviewit.utils.ItemModelMapper;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/items")
public class ItemController {

    private final ItemService itemService;
    private final AuthenticationHelper authenticationHelper;
    private final ItemModelMapper itemModelMapper;

    public ItemController(ItemService itemService, AuthenticationHelper authenticationHelper, ItemModelMapper itemModelMapper) {
        this.itemService = itemService;
        this.authenticationHelper = authenticationHelper;
        this.itemModelMapper = itemModelMapper;
    }

    @GetMapping
    public List<ItemDTO> getAll(@RequestHeader HttpHeaders headers) {
        try {
            authenticationHelper.tryGetUser(headers);
            return itemService.getAll().stream().map(itemModelMapper::itemToDTO).collect(Collectors.toList());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/filter")
    public List<ItemDTO> filter(
            @RequestHeader HttpHeaders headers,
            @RequestParam(required = false) Optional<String> status,
            @RequestParam(required = false) Optional<String> reviewer,
            @RequestParam(required = false) Optional<String> creator,
            @RequestParam(required = false) Optional<String> title,
            @RequestParam(required = false) Optional<Integer> teamID,
            @RequestParam(required = false) Optional<String> searchAll,
            @RequestParam(required = false) Optional<String> sortBy) {

        try {
            authenticationHelper.tryGetUser(headers);
            return itemService.filter(status, reviewer, creator, title, teamID, searchAll, sortBy).stream().map(itemModelMapper::itemToDTO).collect(Collectors.toList());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public ItemDTO getByID(@RequestHeader HttpHeaders headers,
                           @PathVariable int id) {
        try {
            authenticationHelper.tryGetUser(headers);
            return itemModelMapper.itemToDTO(itemService.getByID(id));
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping()
    public void create(@RequestHeader HttpHeaders headers,
                       @Valid @RequestBody ItemDTO itemDTO) {
        try {
            authenticationHelper.tryGetUser(headers);
            Item item = itemModelMapper.itemFromDTO(itemDTO);
            itemService.create(item);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (InvalidUserInputException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public void update(@RequestHeader HttpHeaders headers,
                       @PathVariable int id,
                       @Valid @RequestBody ItemDTO itemDTO) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Item item = itemService.getByID(id);
            item = itemModelMapper.itemFromDTO(itemDTO, item);
            itemService.update(item, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (InvalidUserInputException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        } catch (ForbiddenOperationException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Item item = itemService.getByID(id);
            itemService.delete(item, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (ForbiddenOperationException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
}
