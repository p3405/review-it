package com.telerikacademy.reviewit.controllers.rest;

import com.telerikacademy.reviewit.controllers.AuthenticationHelper;
import com.telerikacademy.reviewit.exceptions.DuplicateEntityException;
import com.telerikacademy.reviewit.exceptions.EntityNotFoundException;
import com.telerikacademy.reviewit.exceptions.ForbiddenOperationException;
import com.telerikacademy.reviewit.exceptions.UnauthorizedOperationException;
import com.telerikacademy.reviewit.models.User;
import com.telerikacademy.reviewit.models.inputDTOs.UserDTO;
import com.telerikacademy.reviewit.models.outputDTOs.DisplayUserDTO;
import com.telerikacademy.reviewit.services.contracts.UserService;
import com.telerikacademy.reviewit.utils.UserModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/users")
public class UserController {

    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;
    private final UserModelMapper mapper;

    @Autowired
    public UserController(UserService service,
                          AuthenticationHelper authenticationHelper,
                          UserModelMapper mapper) {
        this.userService = service;
        this.authenticationHelper = authenticationHelper;
        this.mapper = mapper;
    }

    @GetMapping()
    public List<User> getAll(@RequestHeader HttpHeaders headers) {
        try {
            authenticationHelper.tryGetUser(headers);
            return userService.getAll();
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public DisplayUserDTO getByID(@RequestHeader HttpHeaders headers,
                                  @Valid @PathVariable int id) {
        try {
            authenticationHelper.tryGetUser(headers);
            return mapper.displayUser(userService.getByID(id));
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/filter")
    public List<User> filter(
            @RequestHeader HttpHeaders headers,
            @RequestParam(required = false) Optional<String> username,
            @RequestParam(required = false) Optional<String> email,
            @RequestParam(required = false) Optional<String> phone,
            @RequestParam(required = false) Optional<String> searchAll,
            @RequestParam(required = false) Optional<String> sortBy) {

        try {
            authenticationHelper.tryGetUser(headers);
            return userService.filter(username, email, phone, searchAll, sortBy);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping
    public void create(@RequestHeader HttpHeaders headers,
                       @Valid @RequestBody UserDTO userDTO) {
        try {
            authenticationHelper.tryGetUser(headers);
            User user = mapper.userFromDTO(userDTO);
            userService.create(user);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public void update(@RequestHeader HttpHeaders headers,
                       @PathVariable int id,
                       @Valid @RequestBody UserDTO userDto) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            User userToUpdate = userService.getByID(id);
            user = mapper.userFromDTO(user, userDto);
            userService.update(userToUpdate, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (ForbiddenOperationException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            User userToDelete = userService.getByID(id);
            userService.delete(userToDelete, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (ForbiddenOperationException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        }
    }

}
