package com.telerikacademy.reviewit.controllers.rest;

import com.telerikacademy.reviewit.controllers.AuthenticationHelper;
import com.telerikacademy.reviewit.exceptions.DuplicateEntityException;
import com.telerikacademy.reviewit.exceptions.EntityNotFoundException;
import com.telerikacademy.reviewit.exceptions.ForbiddenOperationException;
import com.telerikacademy.reviewit.exceptions.UnauthorizedOperationException;
import com.telerikacademy.reviewit.models.Item;
import com.telerikacademy.reviewit.models.Team;
import com.telerikacademy.reviewit.models.User;
import com.telerikacademy.reviewit.models.inputDTOs.TeamDTO;
import com.telerikacademy.reviewit.services.contracts.TeamService;
import com.telerikacademy.reviewit.services.contracts.UserService;
import com.telerikacademy.reviewit.utils.TeamModelMapper;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/teams")
public class TeamController {

    private final TeamService teamService;
    private final TeamModelMapper teamModelMapper;
    private final AuthenticationHelper authenticationHelper;
    private final UserService userService;

    public TeamController(TeamService teamService, TeamModelMapper teamModelMapper, AuthenticationHelper authenticationHelper, UserService userService) {
        this.teamService = teamService;
        this.teamModelMapper = teamModelMapper;
        this.authenticationHelper = authenticationHelper;
        this.userService = userService;
    }

    @GetMapping
    public List<Team> getAll(@RequestHeader HttpHeaders headers) {
        try {
            authenticationHelper.tryGetUser(headers);
            return teamService.getAll();
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/{teamID}/members")
    public List<User> getTeamMembers(@RequestHeader HttpHeaders headers,
                                     @Valid @PathVariable int teamID) {
        try {
            authenticationHelper.tryGetUser(headers);
            Team team = teamService.getByID(teamID);
            return teamService.listMembers(team);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/{teamID}/items")
    public List<Item> getTeamItems(@RequestHeader HttpHeaders headers,
                                   @Valid @PathVariable int teamID) {
        try {
            authenticationHelper.tryGetUser(headers);
            Team team = teamService.getByID(teamID);
            return teamService.listItems(team);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public Team getByID(@RequestHeader HttpHeaders headers,
                        @PathVariable int id) {
        try {
            authenticationHelper.tryGetUser(headers);
            return teamService.getByID(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping()
    public void create(@RequestHeader HttpHeaders headers,
                       @Valid @RequestBody TeamDTO teamDTO) {
        try {
            authenticationHelper.tryGetUser(headers);
            Team team = teamModelMapper.teamFromDTO(teamDTO);
            teamService.create(team);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public void update(@RequestHeader HttpHeaders headers,
                       @PathVariable int id,
                       @Valid @RequestBody TeamDTO teamDTO) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Team team = teamService.getByID(id);
            team = teamModelMapper.teamFromDTO(teamDTO, team);
            teamService.update(team, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Team team = teamService.getByID(id);
            teamService.delete(team, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (ForbiddenOperationException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PutMapping("/{teamID}/addMember/{memberID}")
    public void addMemberToTeam(
            @RequestHeader HttpHeaders headers,
            @Valid @PathVariable int teamID,
            @Valid @PathVariable int memberID) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Team team = teamService.getByID(teamID);
            User member = userService.getByID(memberID);
            teamService.addMember(team, member, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{teamID}/removeMember/{memberID}")
    public void removeMemberFromTeam(
            @RequestHeader HttpHeaders headers,
            @Valid @PathVariable int teamID,
            @Valid @PathVariable int memberID) {
        try {
            authenticationHelper.tryGetUser(headers);
            Team team = teamService.getByID(teamID);
            User member = userService.getByID(memberID);
            teamService.removeMember(team, member);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (ForbiddenOperationException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}

