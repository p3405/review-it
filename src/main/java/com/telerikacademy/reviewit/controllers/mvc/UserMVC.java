package com.telerikacademy.reviewit.controllers.mvc;

import com.telerikacademy.reviewit.controllers.AuthenticationHelper;
import com.telerikacademy.reviewit.exceptions.AuthenticationFailureException;
import com.telerikacademy.reviewit.models.User;
import com.telerikacademy.reviewit.models.inputDTOs.UserChangePasswordDTO;
import com.telerikacademy.reviewit.models.inputDTOs.UserDTO;
import com.telerikacademy.reviewit.services.contracts.TeamService;
import com.telerikacademy.reviewit.services.contracts.UserService;
import com.telerikacademy.reviewit.utils.UserModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

@Controller
@RequestMapping("/users")
public class UserMVC extends BaseMVC {

    private final UserService userService;
    private final UserModelMapper userModelMapper;

    @Autowired
    public UserMVC(AuthenticationHelper authenticationHelper,
                   UserService userService,
                   TeamService teamService,
                   UserModelMapper userModelMapper) {
        super(authenticationHelper, teamService);
        this.userService = userService;
        this.userModelMapper = userModelMapper;
    }

    @GetMapping("/profile")
    public String showUserProfile(HttpSession session, Model model) {
        try {
            User user = getAuthenticationHelper().tryGetUser(session);
            UserDTO userDTO = userModelMapper.userToDTO(user);
            model.addAttribute("userDTO", userDTO);
            return "users-profile";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @PostMapping("/profile")
    public String updateProfile(@Valid @ModelAttribute("userDTO") UserDTO userDTO,
                                BindingResult bindingResult,
                                Model model,
                                HttpSession session) {

        if (bindingResult.hasErrors()) {
            return "users-profile";
        }

        User user;
        try {
            user = getAuthenticationHelper().tryGetUser(session);
            User userToUpdate = userModelMapper.userFromDTO(user, userDTO);


            if (userDTO.getProfilePicture().getOriginalFilename().length() != 0) {
                String uploadDir = "profile-pictures/" + user.getID();
                String fileName = StringUtils.cleanPath(userDTO.getProfilePicture().getOriginalFilename());
                user.setPhotoURL(fileName);
                saveFile(uploadDir, fileName, userDTO.getProfilePicture());
            }

            userService.update(user, userToUpdate);


            session.setAttribute("currentUser", user.getUsername());
            return "redirect:/users/profile";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (RuntimeException | IOException e) {
            bindingResult.rejectValue("password", "", e.getMessage());
            model.addAttribute("userDTO", userDTO);
            return "users-profile";
        }
    }

    @GetMapping("/profile/change-password")
    public String showUserChangePassword(HttpSession session, Model model) {
        try {
            getAuthenticationHelper().tryGetUser(session);
            model.addAttribute("userChangePasswordDTO", new UserChangePasswordDTO());
            return "users-profile-change-password";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @PostMapping("/profile/change-password")
    public String updateUserPassword(@Valid @ModelAttribute("userChangePasswordDTO") UserChangePasswordDTO userChangePasswordDTO,
                                     BindingResult bindingResult,
                                     Model model,
                                     HttpSession session) {

        if (bindingResult.hasErrors()) {
            return "users-profile-change-password";
        }

        try {
            User user = getAuthenticationHelper().tryGetUser(session);
            User userToUpdate = userModelMapper.userFromDTO(user, new UserDTO(), userChangePasswordDTO);
            userService.update(user, userToUpdate);
            return "redirect:/users/profile";
        } catch (RuntimeException e) {
            bindingResult.rejectValue("errorPlaceholder", "", e.getMessage());
            model.addAttribute("userChangePasswordDTO", userChangePasswordDTO);
            return "users-profile-change-password";
        }
    }

    private void saveFile(String uploadDir, String fileName, MultipartFile profilePicture) throws IOException {
        Path uploadPath = Paths.get(uploadDir);

        if (!Files.exists(uploadPath)) {
            Files.createDirectories(uploadPath);
        }
        try (InputStream inputStream = profilePicture.getInputStream()) {
            Path filePath = uploadPath.resolve(fileName);
            Files.copy(inputStream, filePath, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            throw new IOException("Could not upload file: " + fileName, e);
        }
    }
}
