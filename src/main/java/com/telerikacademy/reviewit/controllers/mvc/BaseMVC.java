package com.telerikacademy.reviewit.controllers.mvc;

import com.telerikacademy.reviewit.controllers.AuthenticationHelper;
import com.telerikacademy.reviewit.models.Team;
import com.telerikacademy.reviewit.models.User;
import com.telerikacademy.reviewit.services.contracts.TeamService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class BaseMVC {

    private final AuthenticationHelper authenticationHelper;
    private final TeamService teamService;

    public BaseMVC(AuthenticationHelper authenticationHelper, TeamService teamService) {
        this.authenticationHelper = authenticationHelper;
        this.teamService = teamService;
    }

    @ModelAttribute("loggedUser")
    public User populateLoggedUser(HttpSession session) {
        if (session.getAttribute("currentUser") != null) {
            return authenticationHelper.tryGetUser(session);
        }
        return new User();
    }

    @ModelAttribute("loggedUserTeams")
    public List<Team> populateLoggedUserTeam(HttpSession session) {
        if (session.getAttribute("currentUser") != null) {
            User user = authenticationHelper.tryGetUser(session);

            return teamService.getAll()
                    .stream()
                    .filter(team -> team.getMembers().contains(user))
                    .collect(Collectors.toList());
        }
        return new ArrayList<>();
    }

    public AuthenticationHelper getAuthenticationHelper() {
        return authenticationHelper;
    }

    public TeamService getTeamService() {
        return teamService;
    }
}
