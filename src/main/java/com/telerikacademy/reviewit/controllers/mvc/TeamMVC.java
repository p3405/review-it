package com.telerikacademy.reviewit.controllers.mvc;

import com.telerikacademy.reviewit.controllers.AuthenticationHelper;
import com.telerikacademy.reviewit.exceptions.AuthenticationFailureException;
import com.telerikacademy.reviewit.exceptions.DuplicateEntityException;
import com.telerikacademy.reviewit.exceptions.EntityNotFoundException;
import com.telerikacademy.reviewit.exceptions.ForbiddenOperationException;
import com.telerikacademy.reviewit.models.Item;
import com.telerikacademy.reviewit.models.Team;
import com.telerikacademy.reviewit.models.User;
import com.telerikacademy.reviewit.models.inputDTOs.FilterItemDTO;
import com.telerikacademy.reviewit.models.inputDTOs.FilterUserDTO;
import com.telerikacademy.reviewit.models.inputDTOs.SelectedUserDTO;
import com.telerikacademy.reviewit.models.inputDTOs.TeamDTO;
import com.telerikacademy.reviewit.services.contracts.ItemService;
import com.telerikacademy.reviewit.services.contracts.TeamService;
import com.telerikacademy.reviewit.services.contracts.UserService;
import com.telerikacademy.reviewit.utils.TeamModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/teams")
public class TeamMVC extends BaseMVC {

    private final ItemService itemService;
    private final UserService userService;
    private final TeamModelMapper teamModelMapper;

    @Autowired
    public TeamMVC(AuthenticationHelper authenticationHelper,
                   TeamService teamService,
                   ItemService itemService,
                   UserService userService,
                   TeamModelMapper teamModelMapper) {
        super(authenticationHelper, teamService);
        this.itemService = itemService;
        this.userService = userService;
        this.teamModelMapper = teamModelMapper;
    }

    @ModelAttribute("statuses")
    public List<String> populateStatuses() {
        return new ArrayList<>(List.of(
                "Pending",
                "Under Review",
                "Change Requested",
                "Accepted",
                "Rejected"));
    }

    @ModelAttribute("sortParamsMembers")
    public List<String> populateSortParamsMembers() {
        return new ArrayList<>(List.of(
                "ID Ascending",
                "ID Descending",
                "Email Ascending",
                "Email Descending",
                "PhoneNumber Ascending",
                "PhoneNumber Descending"));
    }

    @ModelAttribute("sortParamsItems")
    public List<String> populateSortParamsItems() {
        return new ArrayList<>(List.of(
                "ID Ascending",
                "ID Descending",
                "Title Ascending",
                "Title Descending",
                "Status Ascending",
                "Status Descending"));
    }

    @ModelAttribute("team")
    public Team populateTeam(HttpSession session) {
        if (session.getAttribute("currentUser") != null) {
            User user = getAuthenticationHelper().tryGetUser(session);
            return getTeamService().getAll().stream().filter(team1 -> team1.getMembers().contains(user)).findFirst().orElse(new Team());
        }
        return new Team();
    }

    @ModelAttribute("teamItems")
    public List<Item> populateTeamItems(HttpSession session) {
        if (session.getAttribute("currentUser") != null) {
            User user = getAuthenticationHelper().tryGetUser(session);
            Team team = getTeamService().getAll().stream().filter(team1 -> team1.getMembers().contains(user)).findFirst().orElse(new Team());
            return itemService.getAll().stream().filter(item -> item.getTeam().getId() == team.getId()).collect(Collectors.toList());
        }
        return new ArrayList<>();
    }

    @GetMapping("/{id}")
    public String showTeam(@PathVariable int id, Model model, HttpSession httpSession) {

        try {
            getAuthenticationHelper().tryGetUser(httpSession);
            model.addAttribute("filterUserDTO", new FilterUserDTO());
            model.addAttribute("members", getTeamService().getByID(id).getMembers());
            model.addAttribute("team", getTeamService().getByID(id));
            model.addAttribute("isOwner", isLoggedUserTeamOwner(id, httpSession));
            return "pages-team";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            return "pages-error-404";
        }
    }

    @GetMapping("/new")
    public String showTeamCreation(Model model,
                                   HttpSession httpSession) {
        try {
            User user = getAuthenticationHelper().tryGetUser(httpSession);
            TeamDTO teamDTO = new TeamDTO();
            teamDTO.setOwnerID(user.getID());
            model.addAttribute("teamDTO", teamDTO);
            return "pages-team-new";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @PostMapping("/new")
    public String teamCreation(@Valid @ModelAttribute("teamDTO") TeamDTO teamDTO,
                               BindingResult errors,
                               Model model,
                               HttpSession httpSession) {
        if (errors.hasErrors()) {
            return "pages-team-new";
        }

        try {
            Team team = teamModelMapper.teamFromDTO(teamDTO);
            getTeamService().create(team);
            return "redirect:/";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("name", "", e.getMessage());
            model.addAttribute("teamDTO", teamDTO);
            return "pages-team-new";
        }
    }

    @PostMapping("/{id}")
    public String filterTeamMembers(@PathVariable int id,
                                    @ModelAttribute("filterUserDTO") FilterUserDTO filterUserDTO,
                                    Model model,
                                    HttpSession session) {

        List<User> members = new ArrayList<>(getTeamService().getByID(id).getMembers());

        List<User> filteredMembers = userService.filter(
                Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                filterUserDTO.getSearchAll().length() > 0 ? Optional.of(filterUserDTO.getSearchAll()) : Optional.empty(),
                filterUserDTO.getSortBy().length() > 0 ? Optional.of(filterUserDTO.getSortBy()) : Optional.empty());

        List<User> result = filteredMembers.stream().filter(members::contains).collect(Collectors.toList());

        model.addAttribute("members", result);
        model.addAttribute("isOwner", isLoggedUserTeamOwner(id, session));
        return "pages-team";
    }

    @GetMapping("/{id}/items")
    public String showTeamItems(@PathVariable int id, Model model, HttpSession httpSession) {
        try {
            getAuthenticationHelper().tryGetUser(httpSession);
            getTeamService().getByID(id);
            model.addAttribute("filterItemDTO", new FilterItemDTO());
            model.addAttribute("teamItems", itemService.getAll().stream().filter(item -> item.getTeam().getId() == id).collect(Collectors.toList()));
            return "pages-team-items";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            return "pages-error-404";
        }
    }

    @PostMapping("/{id}/items")
    public String filterTeamItems(@PathVariable int id, @ModelAttribute("filterItemDTO") FilterItemDTO filterItemDTO, Model model, HttpSession session) {

        List<Item> items = itemService.filter(
                Optional.of(filterItemDTO.getStatus()),
                Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.of(id),
                filterItemDTO.getSearchAll().length() > 0 ? Optional.of(filterItemDTO.getSearchAll()) : Optional.empty(),
                filterItemDTO.getSortBy().length() > 0 ? Optional.of(filterItemDTO.getSortBy()) : Optional.empty());

        model.addAttribute("teamItems", items);
        return "pages-team-items";
    }

    @GetMapping("/{teamID}/addMember")
    public String showMemberSelectPage(@PathVariable int teamID, HttpSession session, Model model) {

        try {
            getAuthenticationHelper().tryGetUser(session);

            if (!isLoggedUserTeamOwner(teamID, session)) {
                throw new EntityNotFoundException("Not Found");
            }

            model.addAttribute("users", userService.getAll());
            model.addAttribute("team", getTeamService().getByID(teamID));
            if (!model.containsAttribute("selectedUserDTO")) {
                model.addAttribute("selectedUserDTO", new SelectedUserDTO());
            }
            return "team-select-user";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            return "pages-error-404";
        }
    }

    @PostMapping("/{teamID}/addMember")
    public String showDetailedMemberSelectPage(@PathVariable int teamID,
                                               @ModelAttribute("selectedUserDTO") SelectedUserDTO selectedUserDTO,
                                               HttpSession session,
                                               Model model) {

        try {
            getAuthenticationHelper().tryGetUser(session);

            if (!isLoggedUserTeamOwner(teamID, session)) {
                throw new EntityNotFoundException("Not Found");
            }

            User user = userService.getByID(selectedUserDTO.getUserID());
            model.addAttribute("users", userService.getAll());
            model.addAttribute("team", getTeamService().getByID(teamID));
            model.addAttribute("selectedUser", user);
            model.addAttribute("selectedUserTeams", getTeamService().getAll().stream().filter(team -> team.getMembers().contains(user)).collect(Collectors.toList()));
            model.addAttribute("selectedUserRequests", (long) itemService.filter(Optional.empty(), Optional.empty(), Optional.of(user.getUsername()), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty()).size());
            model.addAttribute("selectedUserReviews", (long) itemService.filter(Optional.empty(), Optional.of(user.getUsername()), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty()).size());
            return "team-select-detailed-user";
        } catch (AuthenticationFailureException e) {
            return "pages-login";
        } catch (EntityNotFoundException e) {
            return "pages-error-404";
        }
    }

    @GetMapping("/{teamID}/addMember/{memberID}")
    public String addMemberToTeam(
            @PathVariable int teamID,
            @PathVariable int memberID,
            @ModelAttribute("selectedUserDTO") SelectedUserDTO selectedUserDTO,
            BindingResult bindingResult,
            RedirectAttributes redirectAttributes,
            HttpSession session) {
        User user;
        Team team;
        try {
            user = getAuthenticationHelper().tryGetUser(session);
            team = getTeamService().getByID(teamID);
            getTeamService().addMember(team, userService.getByID(memberID), user);
            return "redirect:/teams/" + teamID;
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException | ForbiddenOperationException e) {
            return "pages-error-404";
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("userID", "duplicate-user", e.getMessage());
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.selectedUserDTO", bindingResult);
            redirectAttributes.addFlashAttribute("selectedUserDTO", selectedUserDTO);
            return "redirect:/teams/" + teamID + "/addMember";
        }
    }

    @GetMapping("/{teamID}/removeMember/{memberID}")
    public String removeMemberFromTeam(
            @PathVariable int teamID,
            @PathVariable int memberID,
            HttpSession session) {
        User user;
        Team team;
        try {
            user = getAuthenticationHelper().tryGetUser(session);
            team = getTeamService().getByID(teamID);

            if (!team.getOwner().equals(user)) {
                return "pages-error-404";
            }

            getTeamService().removeMember(team, userService.getByID(memberID));
            return "redirect:/teams/" + teamID;
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException | ForbiddenOperationException e) {
            return "pages-error-404";
        }
    }

    private Boolean isLoggedUserTeamOwner(int id, HttpSession httpSession) {
        return getTeamService().getByID(id).getOwner().equals(getAuthenticationHelper().tryGetUser(httpSession));
    }
}
