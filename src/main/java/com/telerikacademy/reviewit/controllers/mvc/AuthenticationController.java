package com.telerikacademy.reviewit.controllers.mvc;

import com.telerikacademy.reviewit.controllers.AuthenticationHelper;
import com.telerikacademy.reviewit.exceptions.AuthenticationFailureException;
import com.telerikacademy.reviewit.exceptions.DuplicateEntityException;
import com.telerikacademy.reviewit.exceptions.InvalidUserInputException;
import com.telerikacademy.reviewit.models.User;
import com.telerikacademy.reviewit.models.inputDTOs.LoginDTO;
import com.telerikacademy.reviewit.models.inputDTOs.RegisterDTO;
import com.telerikacademy.reviewit.services.ConfirmationTokenService;
import com.telerikacademy.reviewit.services.contracts.UserService;
import com.telerikacademy.reviewit.utils.UserModelMapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@RequestMapping("/auth")
public class AuthenticationController {

    private final AuthenticationHelper authenticationHelper;
    private final UserModelMapper userModelMapper;
    private final UserService userService;
    private final ConfirmationTokenService tokenService;

    public AuthenticationController(AuthenticationHelper authenticationHelper, UserModelMapper userModelMapper, UserService userService, ConfirmationTokenService tokenService) {
        this.authenticationHelper = authenticationHelper;
        this.userModelMapper = userModelMapper;
        this.userService = userService;
        this.tokenService = tokenService;
    }

    @GetMapping("/login")
    public String showLoginPage(Model model) {
        model.addAttribute("login", new LoginDTO());
        return "pages-login";
    }

    @PostMapping("/login")
    public String handleLogin(@ModelAttribute("login") LoginDTO login,
                              BindingResult bindingResult,
                              HttpSession session) {

        User user;
        try {
            authenticationHelper.verifyAuthentication(login.getUsername(), login.getPassword());
            session.setAttribute("currentUser", login.getUsername());
            user = authenticationHelper.tryGetUser(session);

            if (!user.isEnabled()) {
                session.removeAttribute("currentUser");
                throw new AuthenticationFailureException("Please verify your email");
            }


            return "redirect:/";
        } catch (AuthenticationFailureException e) {
            bindingResult.rejectValue("password", "", e.getMessage());
            return "pages-login";
        }
    }

    @GetMapping("/register")
    public String showRegisterPage(Model model) {
        model.addAttribute("registerDTO", new RegisterDTO());
        return "pages-register";
    }

    @PostMapping("/register")
    public String handleRegister(@Valid @ModelAttribute("registerDTO") RegisterDTO registerDTO,
                                 BindingResult bindingResult,
                                 RedirectAttributes redirectAttributes) {

        if (bindingResult.hasErrors()) {
            return "pages-register";
        }

        try {
            User user = userModelMapper.userFromRegisterDTO(registerDTO);
            userService.create(user);
            redirectAttributes.addFlashAttribute("newRegistration", true);
            redirectAttributes.addFlashAttribute("login", new LoginDTO());
            return "redirect:/auth/login";
        } catch (DuplicateEntityException | InvalidUserInputException e) {
            bindingResult.rejectValue("passwordConfirm", "auth_error", e.getMessage());
            return "pages-register";
        }
    }

    @GetMapping("/register/confirm")
    public String handleRegister(@RequestParam("token") String token, RedirectAttributes redirectAttributes) {
        tokenService.confirmToken(token);
        redirectAttributes.addFlashAttribute("activatedRegistration", true);
        redirectAttributes.addFlashAttribute("login", new LoginDTO());
        return "redirect:/auth/login";
    }

    @GetMapping("/logout")
    public String handleLogout(HttpSession session) {
        session.removeAttribute("currentUser");
        return "redirect:/";
    }
}
