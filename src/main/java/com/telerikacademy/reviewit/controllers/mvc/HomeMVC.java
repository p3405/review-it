package com.telerikacademy.reviewit.controllers.mvc;

import com.telerikacademy.reviewit.controllers.AuthenticationHelper;
import com.telerikacademy.reviewit.exceptions.AuthenticationFailureException;
import com.telerikacademy.reviewit.models.Item;
import com.telerikacademy.reviewit.models.Team;
import com.telerikacademy.reviewit.models.User;
import com.telerikacademy.reviewit.services.contracts.ItemService;
import com.telerikacademy.reviewit.services.contracts.TeamService;
import com.telerikacademy.reviewit.services.contracts.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/")
public class HomeMVC extends BaseMVC {

    private final UserService userService;
    private final ItemService itemService;

    public HomeMVC(AuthenticationHelper authenticationHelper, UserService userService, TeamService teamService, ItemService itemService) {
        super(authenticationHelper, teamService);
        this.userService = userService;
        this.itemService = itemService;
    }

    @ModelAttribute("users")
    public List<User> populateUsers() {
        return userService.getAll();
    }

    @ModelAttribute("teams")
    public List<Team> populateTeams() {
        return getTeamService().getAll();
    }

    @ModelAttribute("items")
    public List<Item> populateItems() {
        return itemService.getAll();
    }

    @ModelAttribute("itemsCreated")
    public List<Item> populateCreatedItems(HttpSession session) {
        if (session.getAttribute("currentUser") != null) {
            User user = getAuthenticationHelper().tryGetUser(session);
            return itemService.getAll().stream().filter(item -> item.getCreator().getID() == user.getID()).collect(Collectors.toList());
        }
        return new ArrayList<>();
    }

    @ModelAttribute("itemsReviewed")
    public List<Item> populateReviewedItems(HttpSession session) {
        if (session.getAttribute("currentUser") != null) {
            User user = getAuthenticationHelper().tryGetUser(session);
            return itemService.getAll().stream().filter(item -> item.getReviewer().getID() == user.getID()).collect(Collectors.toList());
        }
        return new ArrayList<>();
    }

    @ModelAttribute("team")
    public Team populateTeam(HttpSession session) {
        if (session.getAttribute("currentUser") != null) {
            User user = getAuthenticationHelper().tryGetUser(session);
            return getTeamService().getAll().stream().filter(team1 -> team1.getMembers().contains(user)).findFirst().orElse(new Team());
        }
        return new Team();
    }

    @ModelAttribute("memberTeamsItems")
    public List<Item> populateMemberTeamsItems(HttpSession session) {
        if (session.getAttribute("currentUser") != null) {
            User user = getAuthenticationHelper().tryGetUser(session);
            List<Integer> teams = getTeamService().getAll().stream().filter(team1 -> team1.getMembers().contains(user)).map(Team::getId).collect(Collectors.toList());
            List<Item> items = new ArrayList<>();
            for (int id : teams) {
                items.addAll(itemService.getAll().stream().filter(item -> item.getTeam().getId() == id).collect(Collectors.toList()));
            }
            return items;
        }
        return List.of(new Item());
    }

    @ModelAttribute("teamMembersCount")
    public long populateTeamMembers(HttpSession session) {
        if (session.getAttribute("currentUser") != null) {
            User user = getAuthenticationHelper().tryGetUser(session);
            return getTeamService().getAll().stream().filter(team1 -> team1.getMembers().contains(user)).count();
        }
        return 0;
    }

    @GetMapping
    public String showHomePage(HttpSession session) {
        if (session.getAttribute("currentUser") != null) {
            return "dashboard";
        }
        return "index";
    }

    @GetMapping("/faq")
    public String showFAQ(HttpSession session) {
        try {
            getAuthenticationHelper().tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "pages-login";
        }
        return "faq";
    }

    @GetMapping("/contact")
    public String showContact(HttpSession session) {
        try {
            getAuthenticationHelper().tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "pages-login";
        }
        return "contact-us";
    }
}
