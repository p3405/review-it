package com.telerikacademy.reviewit.controllers.mvc;

import com.telerikacademy.reviewit.controllers.AuthenticationHelper;
import com.telerikacademy.reviewit.exceptions.AuthenticationFailureException;
import com.telerikacademy.reviewit.exceptions.DuplicateEntityException;
import com.telerikacademy.reviewit.exceptions.EntityNotFoundException;
import com.telerikacademy.reviewit.exceptions.InvalidUserInputException;
import com.telerikacademy.reviewit.models.*;
import com.telerikacademy.reviewit.models.inputDTOs.FilterItemDTO;
import com.telerikacademy.reviewit.models.inputDTOs.ItemDTO;
import com.telerikacademy.reviewit.repositories.CRUDRepository;
import com.telerikacademy.reviewit.services.contracts.ItemService;
import com.telerikacademy.reviewit.services.contracts.TeamService;
import com.telerikacademy.reviewit.services.contracts.UserService;
import com.telerikacademy.reviewit.utils.ItemModelMapper;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/items")
public class ItemMVC extends BaseMVC {

    private final ItemService itemService;
    private final UserService userService;
    private final ItemModelMapper itemModelMapper;
    private final CRUDRepository<Status> statusRepository;
    private final CRUDRepository<Comment> commentRepository;

    public ItemMVC(ItemService itemService, TeamService teamService, AuthenticationHelper authenticationHelper, UserService userService, ItemModelMapper itemModelMapper, CRUDRepository<Status> statusRepository, CRUDRepository<Comment> commentRepository) {
        super(authenticationHelper, teamService);
        this.itemService = itemService;
        this.userService = userService;
        this.itemModelMapper = itemModelMapper;
        this.statusRepository = statusRepository;
        this.commentRepository = commentRepository;
        statusRepository.setClassType(Status.class);
        commentRepository.setClassType(Comment.class);
    }

    @ModelAttribute("teams")
    public List<Team> populateTeams() {
        return getTeamService().getAll();
    }

    @ModelAttribute("users")
    public List<User> populateUsers() {
        return userService.getAll();
    }

    @ModelAttribute("statuses")
    public List<Status> populateStatuses() {
        return statusRepository.getAll();
    }

    @ModelAttribute("sortParams")
    public List<String> populateSortParams() {
        return new ArrayList<>(List.of(
                "ID Ascending",
                "ID Descending",
                "Title Ascending",
                "Title Descending",
                "Status Ascending",
                "Status Descending"));
    }

    @GetMapping("/new")
    public String showNewItemPage(Model model, HttpSession session) {
        try {
            getAuthenticationHelper().tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        model.addAttribute("itemDTO", new ItemDTO());
        return "item-create";
    }

    @PostMapping("/new")
    public String createItem(@Valid @ModelAttribute("itemDTO") ItemDTO itemDTO, BindingResult
            errors) {

        if (errors.hasErrors()) {
            return "item-create";
        }

        try {
            Item item = itemModelMapper.itemFromDTO(itemDTO);

            itemService.create(item);

            saveAttachment(itemDTO, item);
            return "redirect:/items/requests";
        } catch (InvalidUserInputException | DuplicateEntityException | IOException e) {
            errors.rejectValue("creatorID", "invalid-item", e.getMessage());
            return "item-create";
        }
    }

    @GetMapping("/{itemID}/update")
    public String showSingleItem(@PathVariable int itemID, Model model, HttpSession session) {
        try {
            getAuthenticationHelper().tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        Item item;
        try {
            item = itemService.getByID(itemID);
        } catch (EntityNotFoundException e) {
            return "pages-error-404";
        }
        model.addAttribute("itemDTO", itemModelMapper.itemToDTO(item));
        model.addAttribute("item", item);
        model.addAttribute("comments", commentRepository.getAll().stream().filter(comment -> comment.getItem().getId() == itemID).collect(Collectors.toList()));
        return "single-item";
    }

    @PostMapping("/{itemID}/update")
    public String updateSingleItem(@Valid @ModelAttribute("itemDTO") ItemDTO itemDTO,
                                   BindingResult errors,
                                   @PathVariable int itemID,
                                   HttpSession session,
                                   Model model) {

        if (errors.hasErrors()) {
            return "single-item";
        }

        User user = getAuthenticationHelper().tryGetUser(session);

        try {
            Item item = itemService.getByID(itemID);

            saveAttachment(itemDTO, item);

            item = itemModelMapper.itemFromDTO(itemDTO, item);

            itemService.update(item, user);

            return "redirect:/items/" + item.getId() + "/update";
        } catch (RuntimeException | IOException e) {
            errors.rejectValue("creatorID", "invalid-title", e.getMessage());
            model.addAttribute("itemDTO", itemDTO);
            model.addAttribute("item", itemService.getByID(itemID));
            model.addAttribute("comments", commentRepository.getAll().stream().filter(comment -> comment.getItem().getId() == itemID).collect(Collectors.toList()));
            return "single-item";
        }
    }

    @GetMapping("/{id}/download-attachments")
    public ResponseEntity<ByteArrayResource> download(@PathVariable int id) throws IOException {
        String path = "attachments/" + id;
        String zipName = System.currentTimeMillis() + ".zip";
        String zipPath = path + "/" + zipName;
        itemService.zipAttachments(path, zipPath, zipName);

        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + zipName);

        Path file = Paths.get(zipPath);
        ByteArrayResource resource = new ByteArrayResource(Files.readAllBytes(file));
        file.toFile().delete();

        return ResponseEntity.ok()
                .headers(headers)
                .contentLength(resource.contentLength())
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .body(resource);
    }

    @GetMapping("/requests")
    public String showAllCreatedItems(Model model, HttpSession session) {
        User user;
        try {
            user = getAuthenticationHelper().tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        model.addAttribute("createdItems", itemService.filter(
                Optional.empty(),
                Optional.empty(),
                Optional.of(user.getUsername()),
                Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.empty()));
        model.addAttribute("filterItemDTO", new FilterItemDTO());
        return "user-requests";
    }

    @PostMapping("/requests")
    public String filterCreatedItems(@ModelAttribute FilterItemDTO filterItemDTO, Model model, HttpSession session) {
        User user = getAuthenticationHelper().tryGetUser(session);

        List<Item> createdItems = itemService.filter(
                Optional.of(filterItemDTO.getStatus()),
                Optional.empty(),
                Optional.of(user.getUsername()),
                Optional.empty(),
                Optional.empty(),
                Optional.of(filterItemDTO.getSearchAll()),
                Optional.of(filterItemDTO.getSortBy()));

        model.addAttribute("currentUser", user);
        model.addAttribute("createdItems", createdItems);
        return "user-requests";
    }

    @GetMapping("/reviews")
    public String showAllReviewedItems(Model model, HttpSession session) {
        User user;
        try {
            user = getAuthenticationHelper().tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        model.addAttribute("reviewedItems", itemService.filter(
                Optional.empty(),
                Optional.of(user.getUsername()),
                Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.empty()));
        model.addAttribute("filterItemDTO", new FilterItemDTO());
        return "user-reviews";
    }

    @PostMapping("/reviews")
    public String filterReviewedItems(@ModelAttribute FilterItemDTO filterItemDTO, Model model, HttpSession session) {
        User user = getAuthenticationHelper().tryGetUser(session);

        List<Item> reviewedItems = itemService.filter(
                Optional.empty(),
                Optional.of(user.getUsername()),
                Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.of(filterItemDTO.getSearchAll()),
                Optional.of(filterItemDTO.getSortBy()));

        model.addAttribute("currentUser", user);
        model.addAttribute("reviewedItems", reviewedItems);
        return "user-reviews";
    }

    @GetMapping("/{itemID}/delete")
    public String deleteItem(@Valid @PathVariable int itemID,
                             @ModelAttribute("filterItemDTO") FilterItemDTO filterItemDTO,
                             BindingResult bindingResult,
                             HttpSession session,
                             Model model) {
        User user;
        try {
            user = getAuthenticationHelper().tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            Item item = itemService.getByID(itemID);
            itemService.delete(item, user);
            return "redirect:/items/requests";
        } catch (InvalidUserInputException e) {
            model.addAttribute("createdItems", itemService.filter(
                    Optional.empty(),
                    Optional.empty(),
                    Optional.of(user.getUsername()),
                    Optional.empty(),
                    Optional.empty(),
                    Optional.empty(),
                    Optional.empty()));
            model.addAttribute("filterItemDTO", new FilterItemDTO());
            bindingResult.rejectValue("status", "invalid.deletion", e.getMessage());
            return "user-requests";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "pages-error-404";
        }
    }

    private void saveAttachment(@ModelAttribute("itemDTO") @Valid ItemDTO itemDTO, Item item) throws IOException {
        if (itemDTO.getAttachment() != null && itemDTO.getAttachment().getOriginalFilename().length() > 0) {
            String fileName = StringUtils.cleanPath(itemDTO.getAttachment().getOriginalFilename());
            String uploadDir = "attachments/" + item.getId();
            itemService.saveFile(uploadDir, fileName, itemDTO.getAttachment());
        }
    }
}
