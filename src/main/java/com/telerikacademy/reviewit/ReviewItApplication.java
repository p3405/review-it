package com.telerikacademy.reviewit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.io.IOException;

@SpringBootApplication
@EnableSwagger2
public class ReviewItApplication {

    public static void main(String[] args) throws IOException {
        SpringApplication.run(ReviewItApplication.class, args);
//        zipAttachments("D:/Coding/Telerik Academy/work-evaluation-management-system/attachments/3",
//                "test.zip");
    }

//    public static void zipAttachments(String sourceDirPath, String zipFilePath) throws IOException {
//        Path p = Files.createFile(Paths.get(zipFilePath));
//        try (ZipOutputStream zs = new ZipOutputStream(Files.newOutputStream(p))) {
//            Path pp = Paths.get(sourceDirPath);
//            Files.walk(pp)
//                    .filter(path -> !Files.isDirectory(path))
//                    .forEach(path -> {
//                        ZipEntry zipEntry = new ZipEntry(pp.relativize(path).toString());
//                        try {
//                            zs.putNextEntry(zipEntry);
//                            Files.copy(path, zs);
//                            zs.closeEntry();
//                        } catch (IOException e) {
//                            System.err.println(e);
//                        }
//                    });
//        }
//    }

}
