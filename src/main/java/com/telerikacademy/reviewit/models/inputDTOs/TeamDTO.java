package com.telerikacademy.reviewit.models.inputDTOs;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class TeamDTO {

    @NotBlank
    @Size(min = 3, max = 30, message = "Team name must be between 3 and 30 symbols.")
    private String name;

    @Positive(message = "Please enter a valid user ID.")
    private int ownerID;

    public TeamDTO() {
    }

    public int getOwnerID() {
        return ownerID;
    }

    public void setOwnerID(int ownerID) {
        this.ownerID = ownerID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
