package com.telerikacademy.reviewit.models.inputDTOs;

import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.io.Serializable;

public class ItemDTO implements Serializable {

    @NotBlank(message = "Title can not be empty.")
    @Size(min = 10, max = 80, message = "Title must be between 10 and 80 symbols.")
    private String title;

    @NotBlank(message = "Description can not be empty.")
    @Size(min = 20, max = 21844, message = "Description must be between 20 and 21844 symbols.")
    private String description;

    @Positive(message = "Team ID must be a positive number.")
    private int teamID;

    @Positive(message = "Creator ID must be a positive number.")
    private int creatorID;

    @Positive(message = "Status ID must be a positive number.")
    private int statusID;

    @Positive(message = "Reviewer ID must be a positive number.")
    private int reviewerID;

    private MultipartFile attachment;

    private String comment;

    public ItemDTO() {
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getReviewerID() {
        return reviewerID;
    }

    public void setReviewerID(int reviewerID) {
        this.reviewerID = reviewerID;
    }

    public int getTeamID() {
        return teamID;
    }

    public void setTeamID(int teamID) {
        this.teamID = teamID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getStatusID() {
        return statusID;
    }

    public void setStatusID(int statusID) {
        this.statusID = statusID;
    }

    public int getCreatorID() {
        return creatorID;
    }

    public void setCreatorID(int creatorID) {
        this.creatorID = creatorID;
    }

    public MultipartFile getAttachment() {
        return attachment;
    }

    public void setAttachment(MultipartFile attachment) {
        this.attachment = attachment;
    }
}
