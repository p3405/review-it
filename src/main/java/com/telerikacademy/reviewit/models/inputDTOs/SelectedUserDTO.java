package com.telerikacademy.reviewit.models.inputDTOs;

public class SelectedUserDTO {

    private int userID;

    public SelectedUserDTO() {
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }
}
