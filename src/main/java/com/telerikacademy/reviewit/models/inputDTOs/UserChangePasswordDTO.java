package com.telerikacademy.reviewit.models.inputDTOs;

import javax.validation.constraints.Pattern;

import static com.telerikacademy.reviewit.models.ModelConstants.PASSWORD_REGEXP;
import static com.telerikacademy.reviewit.models.ModelConstants.PASSWORD_VALIDATION_ERR;

public class UserChangePasswordDTO {


    @Pattern(regexp = PASSWORD_REGEXP, message = PASSWORD_VALIDATION_ERR)
    private String oldPassword;


    @Pattern(regexp = PASSWORD_REGEXP, message = PASSWORD_VALIDATION_ERR)
    private String newPassword;


    @Pattern(regexp = PASSWORD_REGEXP, message = PASSWORD_VALIDATION_ERR)
    private String confirmNewPassword;

    private String errorPlaceholder;

    public UserChangePasswordDTO() {
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getConfirmNewPassword() {
        return confirmNewPassword;
    }

    public void setConfirmNewPassword(String confirmNewPassword) {
        this.confirmNewPassword = confirmNewPassword;
    }

    public String getErrorPlaceholder() {
        return errorPlaceholder;
    }

    public void setErrorPlaceholder(String errorPlaceholder) {
        this.errorPlaceholder = errorPlaceholder;
    }
}
