package com.telerikacademy.reviewit.models.inputDTOs;

public class FilterItemDTO {

    private String searchAll;

    private String status;

    private String sortBy;

    public FilterItemDTO() {
    }

    public String getSearchAll() {
        return searchAll;
    }

    public void setSearchAll(String searchAll) {
        this.searchAll = searchAll;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSortBy() {
        return sortBy;
    }

    public void setSortBy(String sortBy) {
        this.sortBy = sortBy;
    }
}
