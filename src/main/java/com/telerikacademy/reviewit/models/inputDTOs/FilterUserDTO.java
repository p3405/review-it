package com.telerikacademy.reviewit.models.inputDTOs;

public class FilterUserDTO {

    private String searchAll;

    private String sortBy;

    public FilterUserDTO() {
    }

    public String getSearchAll() {
        return searchAll;
    }

    public void setSearchAll(String searchAll) {
        this.searchAll = searchAll;
    }

    public String getSortBy() {
        return sortBy;
    }

    public void setSortBy(String sortBy) {
        this.sortBy = sortBy;
    }
}
