package com.telerikacademy.reviewit.models.inputDTOs;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import static com.telerikacademy.reviewit.models.ModelConstants.PASSWORD_REGEXP;
import static com.telerikacademy.reviewit.models.ModelConstants.PASSWORD_VALIDATION_ERR;

public class LoginDTO {

    @Size(min = 2, message = "Username must be at least 2 symbols long.")
    @NotBlank(message = "Username must be at least 2 symbols long.")
    private String username;

    @Pattern(regexp = PASSWORD_REGEXP, message = PASSWORD_VALIDATION_ERR)
    private String password;

    public LoginDTO() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
