package com.telerikacademy.reviewit.models.inputDTOs;

import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import static com.telerikacademy.reviewit.models.ModelConstants.*;

public class UserDTO {

    @Size(min = 2, max = 20, message = USERNAME_LENGTH_ERR)
    @NotBlank(message = USERNAME_EMPTY_ERR)
    private String username;

    @Size(min = 8, max = 20, message = PASSWORD_LENGTH_ERR)
    @NotBlank(message = PASSWORD_EMPTY_ERR)
    @Pattern(regexp = PASSWORD_REGEXP, message = PASSWORD_VALIDATION_ERR)
    private String password;

    @Email(message = EMAIL_INCORRECT_FORMAT_ERR)
    @NotBlank(message = EMAIL_NULL_ERR)
    private String email;

    @Pattern(regexp = PHONE_REGEXP, message = PHONE_VALIDATION_ERR)
    private String phoneNum;

    private MultipartFile profilePicture;

    public UserDTO() {
    }

    public MultipartFile getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(MultipartFile profilePicture) {
        this.profilePicture = profilePicture;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }
}
