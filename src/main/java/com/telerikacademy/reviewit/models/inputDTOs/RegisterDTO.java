package com.telerikacademy.reviewit.models.inputDTOs;

import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;

import static com.telerikacademy.reviewit.models.ModelConstants.*;

public class RegisterDTO extends LoginDTO {

    @Pattern(regexp = PASSWORD_REGEXP, message = PASSWORD_VALIDATION_ERR)
    private String passwordConfirm;

    @Email(message = INVALID_EMAIL_ERR_MSG)
    private String email;

    @Pattern(regexp = PHONE_REGEXP, message = PHONE_VALIDATION_ERR)
    private String phoneNum;

    public RegisterDTO() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }

}
