package com.telerikacademy.reviewit.models;

public class ModelConstants {

    public static final String EMAIL_NULL_ERR = "Email cannot be null!";
    public static final String EMAIL_INCORRECT_FORMAT_ERR = "Email must be in a valid format!";

    public static final String USERNAME_LENGTH_ERR = "Username must be between 2 and 20 symbols";
    public static final String USERNAME_EMPTY_ERR = "Username can't be empty.";
    public static final String PASSWORD_LENGTH_ERR = "Password must be between 8 and 20 symbols";
    public static final String PASSWORD_EMPTY_ERR = "Password can't be empty.";
    public static final String PASSWORD_REGEXP = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#!.?$%^&-+=()])(?=\\S+$).{8,20}$";
    public static final String PASSWORD_VALIDATION_ERR = "Invalid password format. Must be at least 8 symbols long and should contain a lower case letter, an upper case letter, a digit, a special symbol, and no whitespaces.";
    public static final String PHONE_REGEXP = "[0-9]{10}";
    public static final String PHONE_VALIDATION_ERR = "Phone number should be 10 symbols long and contain nothing but digits.";
    public static final String INVALID_EMAIL_ERR_MSG = "Please enter a valid email.";

}