package com.telerikacademy.reviewit.models.outputDTOs;

public class DisplayUserDTO {

    private int id;

    private String username;

    public DisplayUserDTO() {
    }

    public int getID() {
        return id;
    }

    public void setID(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
