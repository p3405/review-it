package com.telerikacademy.reviewit.repositories;

import com.telerikacademy.reviewit.models.User;
import com.telerikacademy.reviewit.repositories.contracts.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Repository
public class UserRepositoryImpl extends CRUDRepository<User> implements UserRepository<User> {

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
        setClassType(User.class);
    }

    @Override
    public List<User> filter(Optional<String> username, Optional<String> email, Optional<String> phone, Optional<String> searchAll, Optional<String> sortBy) {
        try (Session session = sessionFactory.openSession()) {
            StringBuilder queryString = new StringBuilder(" from User ");
            List<String> filters = new ArrayList<>();
            HashMap<String, Object> params = new HashMap<>();

            username.ifPresent(value -> {
                filters.add(" username like :username ");
                params.put("username", "%" + value + "%");
            });

            email.ifPresent(value -> {
                filters.add(" email like :email ");
                params.put("email", "%" + value + "%");
            });

            phone.ifPresent(value -> {
                filters.add(" phoneNumber like :phoneNumber ");
                params.put("phoneNumber", "%" + value + "%");
            });

            searchAll.ifPresent(value -> {
                filters.add(" (username like :searchAll or email like :searchAll or phoneNumber like :searchAll) ");
                params.put("searchAll", "%" + value + "%");
            });

            if (!filters.isEmpty()) {
                queryString.append(" where ").append(String.join(" and ", filters));
            }

            if (sortBy.isPresent()) {
                if (!sortBy.get().equals("default")) {
                    String sortByQuery = generateSortByQueryString(sortBy);
                    queryString.append(sortByQuery);
                }
            }

            Query<User> query = session.createQuery(queryString.toString(), User.class);
            query.setProperties(params);

            return query.list();
        }
    }

    private String generateSortByQueryString(Optional<String> sortBy) {
        String sortByValue = sortBy.get();
        switch (sortByValue.toLowerCase()) {
            case "id ascending":
                return " order by id";
            case "id descending":
                return " order by id desc";
            case "email ascending":
                return " order by email";
            case "email descending":
                return " order by email desc";
            case "phonenumber ascending":
                return " order by phoneNumber";
            case "phonenumber descending":
                return " order by phoneNumber desc";
            default:
                return "Did not enter switch case";
        }
    }
}
