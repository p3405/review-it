package com.telerikacademy.reviewit.repositories;

import com.telerikacademy.reviewit.models.ConfirmationToken;
import com.telerikacademy.reviewit.repositories.contracts.ConfirmationTokenRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public class ConfirmationTokenRepositoryImpl extends CRUDRepository<ConfirmationToken> implements ConfirmationTokenRepository {

    @Autowired
    public ConfirmationTokenRepositoryImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public Class<ConfirmationToken> getEntityClass() {
        return ConfirmationToken.class;
    }

    @Override
    public Optional<ConfirmationToken> findByToken(String token) {
        try (Session session = retrieveSession()) {
            Query<ConfirmationToken> query = session.createQuery(" from ConfirmationToken where token like :token", getEntityClass());
            query.setParameter("token", token);

            return Optional.of(query.list().get(0));
        }
    }
}
