package com.telerikacademy.reviewit.repositories;

import com.telerikacademy.reviewit.models.Item;
import com.telerikacademy.reviewit.repositories.contracts.ItemRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Repository
public class ItemRepositoryImpl extends CRUDRepository<Item> implements ItemRepository {

    @Autowired
    public ItemRepositoryImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public Class<Item> getEntityClass() {
        return Item.class;
    }


    @Override
    public List<Item> filter(Optional<String> status, Optional<String> reviewer, Optional<String> creator, Optional<String> title, Optional<Integer> teamID, Optional<String> searchAll, Optional<String> sortBy) {
        try (Session session = retrieveSession()) {
            StringBuilder queryString = new StringBuilder(" from Item ");
            List<String> filters = new ArrayList<>();
            HashMap<String, Object> params = new HashMap<>();

            if (status.isPresent()) {
                if (!status.get().equals("default")) {
                    filters.add(" status.name like :status ");
                    params.put("status", status.get());
                }
            }

            teamID.ifPresent(value -> {
                filters.add(" team.id = :id ");
                params.put("id", value);
            });

            reviewer.ifPresent(value -> {
                filters.add(" reviewer.username like :username ");
                params.put("username", "%" + value + "%");
            });

            creator.ifPresent(value -> {
                filters.add(" creator.username like :username ");
                params.put("username", "%" + value + "%");
            });

            title.ifPresent(value -> {
                filters.add(" title like :title ");
                params.put("title", "%" + value + "%");
            });

            if (searchAll.isPresent()) {
                searchAll.ifPresent(value -> {
                    filters.add(" (title like :searchAll or reviewer.username like :searchAll or creator.username like :searchAll or team.name like :searchAll)");
                    params.put("searchAll", "%" + value + "%");
                });
            }

            if (!filters.isEmpty()) {
                queryString.append(" where ")
                        .append(String.join(" and ", filters));
            }

            if (sortBy.isPresent()) {
                if (!sortBy.get().equals("default")) {
                    String sortByQuery = generateSortByQueryString(sortBy);
                    queryString.append(sortByQuery);
                }
            }

            Query<Item> query = session.createQuery(queryString.toString(), getEntityClass());
            query.setProperties(params);

            return query.list();
        }
    }

    private String generateSortByQueryString(Optional<String> sortBy) {
        String sortByValue = sortBy.get();
        switch (sortByValue.toLowerCase()) {
            case "id ascending":
                return " order by id";
            case "id descending":
                return " order by id desc";
            case "title ascending":
                return " order by title";
            case "title descending":
                return " order by title desc";
            case "status ascending":
                return " order by status.id";
            case "status descending":
                return " order by status.id desc";
            default:
                return "Did not enter switch case";
        }
    }
}
