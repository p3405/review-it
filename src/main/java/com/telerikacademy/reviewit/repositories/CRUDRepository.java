package com.telerikacademy.reviewit.repositories;

import com.telerikacademy.reviewit.exceptions.EntityNotFoundException;
import com.telerikacademy.reviewit.repositories.contracts.Repository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.List;

@Component
@Primary
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class CRUDRepository<T extends Serializable> implements Repository<T> {

    protected final SessionFactory sessionFactory;
    private Class<T> classType;

    @Autowired
    public CRUDRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void setClassType(Class<T> type) {
        classType = type;
    }

    public Class<T> getEntityClass() {
        return classType;
    }

    @Override
    public List<T> getAll() {
        try (Session session = retrieveSession()) {
            Query<T> query = session.createQuery(" from " + getEntityClass().getSimpleName() + " ", getEntityClass());
            return query.list();
        }
    }

    @Override
    public T getByID(int id) {
        try (Session session = retrieveSession()) {
            T output = session.get(getEntityClass(), id);
            if (output == null) {
                throw new EntityNotFoundException(getEntityClass().getSimpleName(), id);
            }
            return output;
        }
    }

    @Override
    public void create(T entity) {
        try (Session session = sessionFactory.openSession()) {
            session.save(entity);
        }
    }

    @Override
    public void update(T entity) {
        try (Session session = retrieveSession()) {
            session.beginTransaction();
            session.update(entity);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(T entity) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(entity);
            session.getTransaction().commit();
        }
    }

    public Session retrieveSession() {
        return sessionFactory.openSession();
    }
}
