package com.telerikacademy.reviewit.repositories.contracts;

import com.telerikacademy.reviewit.models.User;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

public interface UserRepository<T extends Serializable> extends Repository<T> {
    List<User> filter(Optional<String> username, Optional<String> email, Optional<String> phone, Optional<String> searchAll, Optional<String> sortBy);
}


