package com.telerikacademy.reviewit.repositories.contracts;

import com.telerikacademy.reviewit.models.Item;

import java.util.List;
import java.util.Optional;

public interface ItemRepository extends Repository<Item> {

    List<Item> filter(Optional<String> status, Optional<String> reviewer, Optional<String> creator, Optional<String> title, Optional<Integer> teamID, Optional<String> searchAll, Optional<String> sortBy);
}
