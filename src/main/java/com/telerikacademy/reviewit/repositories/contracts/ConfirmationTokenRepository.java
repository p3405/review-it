package com.telerikacademy.reviewit.repositories.contracts;

import com.telerikacademy.reviewit.models.ConfirmationToken;

import java.util.Optional;

public interface ConfirmationTokenRepository extends Repository<ConfirmationToken> {

    Optional<ConfirmationToken> findByToken(String token);
}
