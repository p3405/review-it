package com.telerikacademy.reviewit.repositories.contracts;

import java.io.Serializable;
import java.util.List;

public interface Repository<T extends Serializable> {
    void setClassType(Class<T> type);

    List<T> getAll();

    T getByID(int id);

    void create(T entity);

    void update(T entity);

    void delete(T entity);
}
