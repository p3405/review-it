package com.telerikacademy.reviewit.services;

import com.telerikacademy.reviewit.Helpers;
import com.telerikacademy.reviewit.exceptions.DuplicateEntityException;
import com.telerikacademy.reviewit.exceptions.ForbiddenOperationException;
import com.telerikacademy.reviewit.exceptions.InvalidUserInputException;
import com.telerikacademy.reviewit.models.Comment;
import com.telerikacademy.reviewit.models.Item;
import com.telerikacademy.reviewit.models.User;
import com.telerikacademy.reviewit.repositories.CRUDRepository;
import com.telerikacademy.reviewit.repositories.contracts.ItemRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class ItemServiceTests {

    @Mock
    ItemRepository itemRepository;

    @Mock
    CRUDRepository<Comment> commentRepository;

    @InjectMocks
    ItemServiceImpl itemService;

    private Item mockItem;
    private Item mockItemTwo;
    private User mockUser;


    @BeforeEach
    void initializer() {
        mockItem = Helpers.createMockItem();
        mockItemTwo = Helpers.createMockItem();
        mockUser = Helpers.createMockUser();
    }

    @Test
    void getAll_should_callRepository_and_returnValidItem() {
        // Arrange
        Mockito.when(itemRepository.getAll())
                .thenReturn(new ArrayList<>(List.of(mockItem)));

        // Act
        List<Item> items = itemService.getAll();

        // Assert
        Mockito.verify(itemRepository, Mockito.times(1))
                .getAll();
        Assertions.assertEquals(mockItem, items.get(0));
    }

    @Test
    void filter_should_callRepository_and_returnValidItem() {
        // Arrange
        Mockito.when(itemRepository.filter(
                        Optional.empty(),
                        Optional.empty(),
                        Optional.empty(),
                        Optional.empty(),
                        Optional.empty(),
                        Optional.empty(),
                        Optional.empty()))
                .thenReturn(new ArrayList<>(List.of(mockItem)));

        // Act
        List<Item> items = itemService.filter(Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.empty());
        // Assert
        Mockito.verify(itemRepository, Mockito.times(1))
                .filter(Optional.empty(),
                        Optional.empty(),
                        Optional.empty(),
                        Optional.empty(),
                        Optional.empty(),
                        Optional.empty(),
                        Optional.empty());
        Assertions.assertEquals(mockItem, items.get(0));
    }

    @Test
    void getByID_should_callRepository_and_returnValidItem() {
        // Arrange
        Mockito.when(itemRepository.getByID(Mockito.anyInt()))
                .thenReturn(mockItem);

        // Act
        Item item = itemService.getByID(Mockito.anyInt());
        // Assert
        Mockito.verify(itemRepository, Mockito.times(1))
                .getByID(Mockito.anyInt());
        Assertions.assertEquals(mockItem, item);
    }

    @Test
    void create_should_throw_when_itemTitleExists() {
        // Arrange
        Mockito.when(itemRepository.getAll())
                .thenReturn(new ArrayList<>(List.of(mockItem)));

        //Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> itemService.create(mockItem));
    }

    @Test
    void create_should_throw_when_creatorAndReviewerAreTheSameUser() {
        // Arrange
        mockItem.getReviewer().setID(1);
        Mockito.when(itemRepository.getAll())
                .thenReturn(new ArrayList<>());

        //Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class, () -> itemService.create(mockItem));
    }


    @Test
    void create_should_throw_when_itemTeamAndReviewerTeamDiffer() {
        // Arrange
        Mockito.when(itemRepository.getAll())
                .thenReturn(new ArrayList<>());

        //Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class, () -> itemService.create(mockItem));
    }

    @Test
    void create_should_callRepository_when_validItem() {
        // Arrange
        mockItem.getTeam().getMembers().add(mockItem.getReviewer());
        Mockito.when(itemRepository.getAll())
                .thenReturn(new ArrayList<>());

        //Act
        itemService.create(mockItem);

        //Assert
        Mockito.verify(itemRepository, Mockito.times(1))
                .create(mockItem);
    }

    @Test
    void update_should_throw_when_hasChangeInOtherThanStatusAndUserIsNotCreator() {
        // Arrange
        Mockito.when(itemRepository.getByID(Mockito.anyInt()))
                .thenReturn(mockItem);

        mockItemTwo.setTitle("Mock Title Test");
        mockUser.setID(5);

        //Act,Assert
        Assertions.assertThrows(ForbiddenOperationException.class, () -> itemService.update(mockItemTwo, mockUser));
    }

    @Test
    void update_should_throw_when_hasChangeInStatusAndUserIsNotReviewer() {
        // Arrange
        Mockito.when(itemRepository.getByID(Mockito.anyInt()))
                .thenReturn(mockItem);
        mockItemTwo.getStatus().setName("Mock Status Test");
        mockUser.setID(5);

        //Act,Assert
        Assertions.assertThrows(ForbiddenOperationException.class, () -> itemService.update(mockItemTwo, mockUser));
    }

    @Test
    void update_should_throw_when_titleExists() {
        // Arrange
        Mockito.when(itemRepository.getByID(Mockito.anyInt()))
                .thenReturn(mockItem);

        mockItemTwo.setId(10);
        Mockito.when(itemRepository.getAll())
                .thenReturn(new ArrayList<>(List.of(mockItemTwo)));


        //Act,Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> itemService.update(mockItem, mockUser));
    }

    @Test
    void update_should_throw_when_hasChangeInTeam() {
        // Arrange
        Mockito.when(itemRepository.getByID(Mockito.anyInt()))
                .thenReturn(mockItem);


        Mockito.when(itemRepository.getAll())
                .thenReturn(new ArrayList<>());

        mockItemTwo.getTeam().setId(10);

        //Act,Assert
        Assertions.assertThrows(ForbiddenOperationException.class, () -> itemService.update(mockItemTwo, mockUser));
    }

    @Test
    void update_should_throw_when_creatorAndReviewerAreTheSameUser() {
        // Arrange
        Mockito.when(itemRepository.getByID(Mockito.anyInt()))
                .thenReturn(mockItem);


        Mockito.when(itemRepository.getAll())
                .thenReturn(new ArrayList<>());
        mockItemTwo.getReviewer().setID(1);


        //Act,Assert
        Assertions.assertThrows(InvalidUserInputException.class, () -> itemService.update(mockItemTwo, mockUser));
    }

    @Test
    void update_should_throw_when_itemTeamDiffersFromReviewerTeam() {
        // Arrange
        Mockito.when(itemRepository.getByID(Mockito.anyInt()))
                .thenReturn(mockItem);


        Mockito.when(itemRepository.getAll())
                .thenReturn(new ArrayList<>());

        //Act,Assert
        Assertions.assertThrows(InvalidUserInputException.class, () -> itemService.update(mockItemTwo, mockUser));
    }

    @Test
    void update_should_throw_when_hasChangeInStatusToRejectedAndNoComment() {
        // Arrange
        Mockito.when(itemRepository.getByID(Mockito.anyInt()))
                .thenReturn(mockItem);

        Mockito.when(itemRepository.getAll())
                .thenReturn(new ArrayList<>());

        mockItemTwo.getTeam().getMembers().add(mockItem.getReviewer());
        mockItemTwo.getStatus().setName("Rejected");
        mockItemTwo.setComment("");
        mockUser.setID(2);

        //Act,Assert
        Assertions.assertThrows(InvalidUserInputException.class, () -> itemService.update(mockItemTwo, mockUser));
    }

    @Test
    void update_should_callRepository_when_validChange() {
        // Arrange
        Mockito.when(itemRepository.getByID(Mockito.anyInt()))
                .thenReturn(mockItem);

        Mockito.when(itemRepository.getAll())
                .thenReturn(new ArrayList<>());

        Mockito.doNothing().when(commentRepository).create(Mockito.any());

        mockItemTwo.getTeam().getMembers().add(mockItem.getReviewer());
        mockItemTwo.getStatus().setName("Rejected");
        mockUser.setID(2);
        mockItemTwo.setComment("Test Comment");

        //Act
        itemService.update(mockItemTwo, mockUser);

        //Assert
        Mockito.verify(itemRepository, Mockito.times(1))
                .update(mockItemTwo);
    }

    @Test
    void delete_should_throw_when_userIsNotCreator() {
        // Arrange
        mockUser.setID(10);

        //Act,Assert
        Assertions.assertThrows(ForbiddenOperationException.class, () -> itemService.delete(mockItem, mockUser));
    }

    @Test
    void delete_should_callRepository_when_userIsCreator() {
        // Arrange,Act
        itemService.delete(mockItem, mockUser);

        //Assert
        Mockito.verify(itemRepository, Mockito.times(1))
                .delete(mockItem);
    }
}
