package com.telerikacademy.reviewit.services;

import com.telerikacademy.reviewit.exceptions.DuplicateEntityException;
import com.telerikacademy.reviewit.exceptions.EntityNotFoundException;
import com.telerikacademy.reviewit.exceptions.ForbiddenOperationException;
import com.telerikacademy.reviewit.models.Item;
import com.telerikacademy.reviewit.models.Team;
import com.telerikacademy.reviewit.models.User;
import com.telerikacademy.reviewit.repositories.contracts.Repository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static com.telerikacademy.reviewit.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class TeamServiceTests {

    @Mock
    Repository<Team> mockRepo;

    @InjectMocks
    TeamServiceImpl teamService;

    private static User mockUser;
    private static Item mockItem;
    private static List<Team> teams;
    private static Team mockTeam;

    @BeforeEach
    void initializer() {
        mockRepo.setClassType(Team.class);
        mockUser = createMockUser();
        mockItem = createMockItem();
        mockTeam = createMockTeam();
        teams = new ArrayList<>();
        teams.add(mockTeam);
    }

    @Test
    public void getAll_should_callRepository() {
        //Arrange
        Mockito.when(mockRepo.getAll()).thenReturn(teams);

        // Act
        teamService.getAll();

        // Assert
        Assertions.assertEquals(mockTeam, teamService.getAll().get(0));
        Mockito.verify(mockRepo, Mockito.times(2))
                .getAll();
    }

    @Test
    void getByID_should_returnTeam_when_IDFound() {
        // Arrange
        Mockito.when(mockRepo.getByID(Mockito.anyInt())).thenReturn(mockTeam);

        // Act
        Team result = teamService.getByID(mockTeam.getId());

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockTeam.getId(), result.getId()),
                () -> Assertions.assertEquals(mockTeam.getName(), result.getName()),
                () -> Assertions.assertEquals(mockTeam.getOwner().getID(), result.getOwner().getID()),
                () -> Assertions.assertEquals(mockTeam.isDeleted(), result.isDeleted())
        );
    }

    @Test
    void getByID_should_throw_when_IDNotFound() {
        // Arrange
        Mockito.when(mockRepo.getByID(1)).thenReturn(mockTeam);
        Mockito.when(mockRepo.getByID(2)).thenThrow(EntityNotFoundException.class);

        // Act,Assert
        Assertions.assertEquals(mockTeam, mockRepo.getByID(1));
        Assertions.assertThrows(EntityNotFoundException.class, () -> teamService.getByID(2));
    }

    @Test
    void create_should_throw_when_NameIsTaken() {
        // Arrange
        Mockito.when(mockRepo.getAll()).thenReturn(teams);

        Team team = createMockTeam();
        team.setName(mockTeam.getName());

        // Act,Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> teamService.create(team));
    }

    @Test
    void create_should_createTeam_when_noValidationsFail() {
        // Arrange
        Team team = createMockTeam();
        teamService.removeMember(team, mockUser);

        // Act
        teamService.create(team);

        // Assert
        Mockito.verify(mockRepo, Mockito.times(1)).create(team);
    }

    @Test
    void update_should_throw_when_issuerNotTeamOwner() {
        // Arrange
        User issuer = createMockUser();
        issuer.setID(2);

        Mockito.when(mockRepo.getByID(Mockito.anyInt()))
                .thenReturn(mockTeam);

        // Act, Assert
        Assertions.assertThrows(ForbiddenOperationException.class, () -> teamService.update(mockTeam, issuer));
    }

    @Test
    void update_should_throw_when_teamNameTaken() {
        // Arrange
        Mockito.when(mockRepo.getAll()).thenReturn(teams);
        User issuer = createMockUser();
        Team team = createMockTeam();
        team.setId(2);

        Mockito.when(mockRepo.getByID(Mockito.anyInt()))
                .thenReturn(mockTeam);

        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> teamService.update(team, issuer));
    }

    @Test
    void update_should_throw_when_hasChangeInOwner() {
        // Arrange
        mockUser.setID(10);
        mockTeam.setOwner(mockUser);
        Mockito.when(mockRepo.getByID(Mockito.anyInt()))
                .thenReturn(mockTeam);

        Team mockTeamUpdate = createMockTeam();

        // Act, Assert
        Assertions.assertThrows(ForbiddenOperationException.class, () -> teamService.update(mockTeamUpdate, mockTeamUpdate.getOwner()));
    }

    @Test
    void update_should_updateTeam() {
        // Arrange
        User issuer = createMockUser();

        Mockito.when(mockRepo.getByID(Mockito.anyInt()))
                .thenReturn(mockTeam);

        // Act
        teamService.update(mockTeam, issuer);

        // Assert
        Mockito.verify(mockRepo, Mockito.times(1)).update(mockTeam);
    }

    @Test
    void delete_should_throw_when_issuerNotTeamOwner() {
        // Arrange
        User issuer = createMockUser();
        issuer.setID(2);

        // Act, Assert
        Assertions.assertThrows(ForbiddenOperationException.class, () -> teamService.delete(mockTeam, issuer));
    }

    @Test
    void delete_should_throw_when_teamHasMembers() {
        // Arrange
        User issuer = createMockUser();

        // Act, Assert
        Assertions.assertThrows(ForbiddenOperationException.class, () -> teamService.delete(mockTeam, issuer));
    }

    @Test
    void delete_should_callRepository_when_teamEligibleForDeletion() {
        // Arrange
        User issuer = mockTeam.getOwner();
        teamService.removeMember(mockTeam, mockUser);

        // Act
        teamService.delete(mockTeam, issuer);

        // Assert
        Mockito.verify(mockRepo, Mockito.times(1)).delete(mockTeam);
    }

    @Test
    void addMember_should_throw_when_userIsNotCreator() {
        //Arrange
        mockUser.setID(10);
        // Act, Assert
        Assertions.assertThrows(ForbiddenOperationException.class, () -> teamService.addMember(mockTeam, mockTeam.getOwner(), mockUser));
    }

    @Test
    void addMember_should_throw_when_memberAlreadyInTeam() {
        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> teamService.addMember(mockTeam, mockTeam.getOwner(), mockTeam.getOwner()));
    }

    @Test
    void addMember_should_addMember_when_validMemberPassed() {
        // Arrange
        User newMember = createMockUser();
        newMember.setID(2);
        newMember.setUsername("newUsername");
        newMember.setEmail("newMail@mail.com");

        // Act
        teamService.addMember(mockTeam, newMember, mockTeam.getOwner());

        // Assert
        Assertions.assertEquals(2, mockTeam.getMembers().size());
    }

    @Test
    void removeMember_should_throw_when_memberIsNotInTeam() {
        // Arrange
        User forRemoval = createMockUser();
        forRemoval.setID(2);

        // Act, Assert
        Assertions.assertThrows(ForbiddenOperationException.class, () -> teamService.removeMember(mockTeam, forRemoval));
    }

    @Test
    void removeMember_should_removeMember_when_validMemberPassed() {
        // Arrange
        User newMember = createMockUser();
        newMember.setID(2);

        // Act
        teamService.addMember(mockTeam, newMember, mockTeam.getOwner());
        teamService.removeMember(mockTeam, mockTeam.getOwner());

        // Assert
        Assertions.assertEquals(1, mockTeam.getMembers().size());
        Assertions.assertTrue(mockTeam.getMembers().stream().anyMatch(member -> member.getID() == 2));
    }

    @Test
    void listMembers_should_listAllMembers() {
        // Arrange
        User newMember = createMockUser();
        newMember.setID(2);

        // Act
        teamService.addMember(mockTeam, newMember, mockTeam.getOwner());

        // Assert
        Assertions.assertEquals(2, teamService.listMembers(mockTeam).size());
    }
}
