package com.telerikacademy.reviewit.services;

import com.telerikacademy.reviewit.exceptions.DuplicateEntityException;
import com.telerikacademy.reviewit.exceptions.EntityNotFoundException;
import com.telerikacademy.reviewit.exceptions.ForbiddenOperationException;
import com.telerikacademy.reviewit.models.Item;
import com.telerikacademy.reviewit.models.Team;
import com.telerikacademy.reviewit.models.User;
import com.telerikacademy.reviewit.repositories.UserRepositoryImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static com.telerikacademy.reviewit.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class UserServiceTests {

    @Mock
    UserRepositoryImpl mockRepo;

    @Mock
    ItemServiceImpl itemService;

    @Mock
    ConfirmationTokenService tokenService;

    @Mock
    EmailService emailService;

    @Mock
    TeamServiceImpl teamService;

    @InjectMocks
    UserServiceImpl userService;

    private static User mockUser;
    private static List<User> users;
    private static Item mockItem;
    private static Team mockTeam;

    @BeforeEach
    void initializer() {
        mockUser = createMockUser();
        users = new ArrayList<>();
        users.add(mockUser);
        mockItem = createMockItem();
        mockTeam = createMockTeam();
    }

    @Test
    public void getAll_should_callRepository() {
        //Arrange
        Mockito.when(mockRepo.getAll()).thenReturn(users);

        // Act
        userService.getAll();

        // Assert
        Assertions.assertEquals(mockUser, userService.getAll().get(0));
        Mockito.verify(mockRepo, Mockito.times(2))
                .getAll();
    }

    @Test
    void getByID_should_returnUser_when_IDFound() {
        // Arrange
        Mockito.when(mockRepo.getByID(Mockito.anyInt())).thenReturn(mockUser);

        // Act
        User result = userService.getByID(mockUser.getID());

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockUser.getID(), result.getID()),
                () -> Assertions.assertEquals(mockUser.getUsername(), result.getUsername()),
                () -> Assertions.assertEquals(mockUser.getPassword(), result.getPassword()),
                () -> Assertions.assertEquals(mockUser.getEmail(), result.getEmail()),
                () -> Assertions.assertEquals(mockUser.getPhoneNumber(), result.getPhoneNumber()),
                () -> Assertions.assertEquals(mockUser.getPhotoURL(), result.getPhotoURL()),
                () -> Assertions.assertEquals(mockUser.isDeleted(), result.isDeleted())
        );
    }

    @Test
    void getByID_should_throw_when_IDNotFound() {
        // Arrange
        Mockito.when(mockRepo.getByID(1)).thenReturn(mockUser);
        Mockito.when(mockRepo.getByID(2)).thenThrow(EntityNotFoundException.class);

        // Act,Assert
        Assertions.assertEquals(mockUser, mockRepo.getByID(1));
        Assertions.assertThrows(EntityNotFoundException.class, () -> userService.getByID(2));
    }

    @Test
    void create_should_throw_when_UsernameIsTaken() {
        // Arrange
        Mockito.when(mockRepo.getAll()).thenReturn(users);

        User user = createMockUser();
        user.setEmail("newMail@mail.com");
        user.setPhoneNumber("1111111111");

        // Act,Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> userService.create(user));
    }

    @Test
    void create_should_throw_when_PhoneIsTaken() {
        // Arrange
        Mockito.when(mockRepo.getAll()).thenReturn(users);

        User user = createMockUser();
        user.setUsername("testUsername");
        user.setEmail("newMail@mail.com");

        // Act,Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> userService.create(user));
    }

    @Test
    void create_should_throw_when_EmailIsTaken() {
        // Arrange
        Mockito.when(mockRepo.getAll()).thenReturn(users);

        User user = createMockUser();
        user.setUsername("testUsername");
        user.setPhoneNumber("1111111111");

        // Act,Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> userService.create(user));
    }

    @Test
    void create_should_createUser_when_noValidationsFail() {
        // Act
        userService.create(mockUser);

        // Assert
        Mockito.verify(mockRepo, Mockito.times(1)).create(mockUser);
    }

    @Test
    void update_should_throw_when_issuerNotAccountOwner() {
        // Arrange
        User issuer = createMockUser();
        issuer.setID(2);

        // Act, Assert
        Assertions.assertThrows(ForbiddenOperationException.class, () -> userService.update(mockUser, issuer));
    }

    @Test
    void update_should_updateUser() {
        // Arrange
        User issuer = createMockUser();

        // Act
        userService.update(mockUser, issuer);

        // Assert
        Mockito.verify(mockRepo, Mockito.times(1)).update(mockUser);
    }

    @Test
    void delete_should_throw_when_userIsItemCreatorOrReviewer() {
        // Arrange
        Mockito.when(itemService.getAll()).thenReturn(List.of(mockItem));
        User issuer = createMockUser();

        // Act, Assert
        Assertions.assertThrows(ForbiddenOperationException.class, () -> userService.delete(mockUser, issuer));
    }

    @Test
    void delete_should_throw_when_userTeamCreator() {
        // Arrange
        Mockito.when(teamService.getAll()).thenReturn(List.of(mockTeam));
        User issuer = createMockUser();

        // Act, Assert
        Assertions.assertThrows(ForbiddenOperationException.class, () -> userService.delete(mockUser, issuer));
    }

    @Test
    void delete_should_callRepository_when_userEligibleForDeletion() {
        // Arrange
        Mockito.when(itemService.getAll()).thenReturn(new ArrayList<>());
        Mockito.when(teamService.getAll()).thenReturn(new ArrayList<>());
        User issuer = createMockUser();

        // Act
        userService.delete(mockUser, issuer);

        // Assert
        Mockito.verify(mockRepo, Mockito.times(1)).delete(mockUser);
    }

    @Test
    void filter_should_callRepository() {
        // Arrange
        Mockito.when(mockRepo.filter(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(new ArrayList<>());

        // Act
        userService.filter(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any());

        // Assert
        Mockito.verify(mockRepo, Mockito.times(1))
                .filter(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any());
    }
}
