package com.telerikacademy.reviewit;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.telerikacademy.reviewit.models.Item;
import com.telerikacademy.reviewit.models.Status;
import com.telerikacademy.reviewit.models.Team;
import com.telerikacademy.reviewit.models.User;

import java.util.HashSet;
import java.util.Set;

public class Helpers {

    public static Item createMockItem() {
        var mockItem = new Item();
        mockItem.setId(1);
        mockItem.setTitle("Mock Item Title");
        mockItem.setDescription("Mock Item Description");
        mockItem.setTeam(createMockTeam());
        mockItem.setCreator(createMockUser());
        User reviewer = createMockUser();
        reviewer.setID(2);
        mockItem.setReviewer(reviewer);
        mockItem.setStatus(createMockStatus());
        mockItem.setDeleted(false);
        return mockItem;
    }

    public static User createMockUser() {
        var mockUser = new User();
        mockUser.setID(1);
        mockUser.setUsername("MockUsername");
        mockUser.setPassword("MockLastName");
        mockUser.setEmail("mockEmail@email.com");
        mockUser.setPhoneNumber("0000000000");
        mockUser.setPhotoURL("mockPhotoURL");
        mockUser.setDeleted(false);
        return mockUser;
    }

    public static Team createMockTeam() {
        var mockTeam = new Team();
        mockTeam.setId(1);
        mockTeam.setName("Mock Team Name");
        mockTeam.setOwner(createMockUser());
        mockTeam.setDeleted(false);
        Set<User> members = new HashSet<>();
        members.add(mockTeam.getOwner());
        mockTeam.setMembers(members);
        return mockTeam;
    }

    private static Status createMockStatus() {
        var status = new Status();
        status.setId(1);
        status.setName("Mock Status Name");
        return status;
    }

    /**
     * Accepts an object and returns the stringified object.
     * Useful when you need to pass a body to a HTTP request.
     */
    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
